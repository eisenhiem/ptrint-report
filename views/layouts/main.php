<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ระบบพิมพ์รายงานเอกสาร',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-dark bg-danger navbar-expand-md',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'nav-pills justify-content-end'],
        'items' => [
            ['label' => 'Log Out','visible' => !Yii::$app->user->isGuest, 'url' => ['/user/security/logout'],'linkOptions' => ['data-method' => 'post']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
<div class="container-fluid">
        <div class="row">
            <div class="col-6">
                &copy; โรงพยาบาลเหล่าเสือโก้ก <?= date('Y') ?>
            </div>
            <div class="col-6 text-right">
                <?=Html::a('Mr.Jakkrapong Wongkamalasai', 'https://gitlab.com/eisenhiem/print-report', ['target' => '_blank', 'title' => 'พัฒนาโดยนายจักรพงษ์ วงศ์กมลาไสย'])?>
            </div>
        </div>

    </div></footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
