<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KiosPttype */

$this->title = 'เพิ่มรายการ Authen Code';

?>
<div class="kios-pttype-create">
    <h3 align="center"><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
