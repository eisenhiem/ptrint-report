<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KiosPttypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kios-pttype-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cid') ?>

    <?= $form->field($model, 'json_data') ?>

    <?= $form->field($model, 'claimCode') ?>

    <?= $form->field($model, 'claimType') ?>

    <?php // echo $form->field($model, 'cln') ?>

    <?php // echo $form->field($model, 'regist_date') ?>

    <?php // echo $form->field($model, 'regist_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
