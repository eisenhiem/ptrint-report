<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KiosPttype */

$this->title = 'Update Authen: ' . $model->id;

?>
<div class="kios-pttype-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
