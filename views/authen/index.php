<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KiosPttypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Authen Code';
?>
<div class="kios-pttype-index">

    <p>
        <?= Html::a('เพิ่มรายการ Authen Code', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'cid',
            //'json_data:ntext',
            'claimCode',
            'claimType',
            //'cln',
            'regist_date',
            'regist_time',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
