<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KiosPttype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kios-pttype-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="card col-md-6 offset-md-3 col-sm-10 offset-sm-1">
            <div>
                <?= $form->field($model, 'cid')->textInput(['maxlength' => true]) ?>
            </div>
            <div>
                <?= $form->field($model, 'claimCode')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('บันทึก Authen code', ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
</div>