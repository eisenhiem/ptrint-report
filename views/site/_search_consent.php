<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['consent'],
        'method' => 'get',
    ]); ?>
        <div class="row">
            <div class="col-md-2 col-sm-6">
                <?= $form->field($model, 'hn')->textInput(['placeholder' => 'HN'])->label(false) ?>
            </div>
            <div class="col-md-3 col-sm-6">
            <?= $form->field($model, 'pop_id')->textInput(['placeholder' => 'เลขบัตรประชาชน'])->label(false) ?>                
            </div>
            <div class="col-md-3 col-sm-5">
                <?= $form->field($model, 'fname')->textInput(['placeholder' => 'ชื่อ'])->label(false) ?>                
            </div>
            <div class="col-md-3 col-sm-6">
            <?= $form->field($model, 'lname')->textInput(['placeholder' => 'สกุล'])->label(false) ?>
            </div>
            <div class="col-md-1 col-sm-1">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

</div>
