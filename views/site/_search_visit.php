<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ItemsSearch */
/* @var $form yii\widgets\ActiveForm */

$m = [
    1 => 'มกราคม', 2 => 'กุมภาพันธ์', 3 => 'มีนาคม', 4 => 'เมษายน', 5 => 'พฤษภาคม', 6 => 'มิถุนายน', 7 => 'กรกฎราคม', 8 => 'สิงหาคม', 9 => 'กันยายน', 10 => 'ตุลาคม', 11 => 'พฤศจิกายน', 12 => 'ธันวาคม'
];
$y = [
    '2019' => '2562',
    '2020' => '2563',
    '2021' => '2564',
    '2022' => '2565',
    '2023' => '2566',
    '2024' => '2567',
]
?>

<div class="items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['visit', 'id' => $id],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-2 col-sm-2">
        <?= $form->field($model, 'an')->textInput(['placeholder' => 'AN'])->label(false) ?>
        </div>
        <div class="col-md-2 col-sm-2">
        <?= $form->field($model, 'm')->dropDownList($m,['prompt'=>'เลือกเดือน'])->label(false) ?>
        </div>
        <div class="col-md-2 col-sm-2">
        <?= $form->field($model, 'y')->dropDownList($y,['prompt'=>'เลือกปี'])->label(false) ?>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>