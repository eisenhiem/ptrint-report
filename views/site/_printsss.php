<?php

use yii\helpers\Html;
use app\models\Prscdt;
use app\models\Income;
use app\models\Pillness;
use app\models\Sign;

function thaimonth($m)
{
    switch ($m) {
        case '01':
            $thainame = 'มกราคม';
            break;
        case '02':
            $thainame = 'กุมภาพันธ์';
            break;
        case '03':
            $thainame = 'มีนาคม';
            break;
        case '04':
            $thainame = 'เมษายน';
            break;
        case '05':
            $thainame = 'พฤษภาคม';
            break;
        case '06':
            $thainame = 'มิถุนายน';
            break;
        case '07':
            $thainame = 'กรกฎาคม';
            break;
        case '08':
            $thainame = 'สิงหาคม';
            break;
        case '09':
            $thainame = 'กันยายน';
            break;
        case '10':
            $thainame = 'ตุลาคม';
            break;
        case '11':
            $thainame = 'พฤศจิกายน';
            break;
        case '12':
            $thainame = 'ธันวาคม';
            break;
    }
    return $thainame;
}

function num2wordsThai($num)
{
    $num = str_replace(",", "", $num);
    $num_decimal = explode(".", $num);
    $num = $num_decimal[0];
    $returnNumWord = '';
    $lenNumber = strlen($num);
    $lenNumber2 = $lenNumber - 1;
    $kaGroup = array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
    $kaDigit = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
    $kaDigitDecimal1 = array("", "สิบ", "ยี่สิบ", "สามสิบ", "สี่สิบ", "ห้าสิบ", "หกสิบ", "เจ็ตสิบ", "แปดสิบ", "เก้าสิบ");
    $kaDigitDecimal2 = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
    $ii = 0;
    for ($i = $lenNumber2; $i >= 0; $i--) {
        $kaNumWord[$i] = substr($num, $ii, 1);
        $ii++;
    }
    $ii = 0;
    for ($i = $lenNumber2; $i >= 0; $i--) {
        if (($kaNumWord[$i] == 2 && $i == 1) || ($kaNumWord[$i] == 2 && $i == 7)) {
            $kaDigit[$kaNumWord[$i]] = "ยี่";
        } else {
            if ($kaNumWord[$i] == 2) {
                $kaDigit[$kaNumWord[$i]] = "สอง";
            }
            if (($kaNumWord[$i] == 1 && $i <= 2 && $i == 0) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 6)) {
                if ($kaNumWord[$i + 1] == 0) {
                    $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                } else {
                    $kaDigit[$kaNumWord[$i]] = "เอ็ด";
                }
            } elseif (($kaNumWord[$i] == 1 && $i <= 2 && $i == 1) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 7)) {
                $kaDigit[$kaNumWord[$i]] = "";
            } else {
                if ($kaNumWord[$i] == 1) {
                    $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
                }
            }
        }
        if ($kaNumWord[$i] == 0) {
            if ($i != 6) {
                $kaGroup[$i] = "";
            }
        }
        $kaNumWord[$i] = substr($num, $ii, 1);
        $ii++;
        $returnNumWord .= $kaDigit[$kaNumWord[$i]] . $kaGroup[$i];
    }
    if (isset($num_decimal[1])) {
        $returnNumWord .= "บาท";
        if ($num_decimal[1] == "00") {
            $returnNumWord .= "ถ้วน";
        } else {
            for ($i = 0; $i < strlen($num_decimal[1]); $i++) {
                $returnNumWord .= $kaDigitDecimal1[substr($num_decimal[1], $i, 1)];
                if ($i == 0) {
                    $returnNumWord .= $kaDigitDecimal2[substr($num_decimal[1], $i + 1, 1)];
                }
            }
            $returnNumWord .= "สตางค์";
        }
    }
    return $returnNumWord;
}

$total = 0;
$c = [];
foreach ($model as $cost) {
    for ($i = 1; $i <= 23; $i++) {
        if ($i < 10) {
            $j = '0' . $i;
        } else {
            $j = "" . $i . "";
        }
        if ($cost->income == $j) {
            if ($i == 5 && $cost->cgd == '') {
                $c[$i] = $c[$i] + 0;
                $total = $total + $cost->rcptamt;
            } else {
                $c[$i] = $c[$i] + $cost->rcptamt;
                $total = $total + $cost->rcptamt;
            }
        }
    }
}

$list = Income::find()->All();
?>

<h3 align="center">โรงพยาบาลเหล่าเสือโก้ก จังหวัดอุบลราชธานี<br>แบบฟอร์มสรุปค่ารักษาพยาบาลและใบแจ้งหนี้ค่ารักษาพยาบาลผู้ป่วยประกันสังคม</h3>
<b>ชื่อหน่วยบริการ : </b>โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี<br>
<b>ชื่อ-สกุล : </b><?= $pt->getFullName() ?> <b>วันเกิด : </b><?= $pt->getฺBirthdate() ?> <b>อายุ : </b><?= $pt->getAge() ?><br>
<b> HN : </b><?= $pt->hn ?> <b> AN : </b><?= $visit->an ?> <b> อาชีพ : </b><?= $pt->job->nameoccptn ?><br>
<b>โรงพยาบาลคู่สัญญาหลัก : </b><?= $insure->getHospName() ?><br>
<?php if ($ipt) { ?>
    <b>หอผู้ป่วย : </b><?= $ipt->wardadmit->nameidpm ?>
    <b>วันที่รับไว้ : </b><?= $visit->getVisitDate() ?>
    <b>วันที่จำหน่าย : </b><?= $ipt ? $ipt->getDcDate() : '-'; ?>
    <b>รวม : </b><?= $ipt ? $ipt->daycnt : '-'; ?> วัน<br>
<?php
} else {
    echo '<b>วันที่รักษา : </b>' . $visit->getVisitDate() . '<br>';
}
?>
<b>การวินิจฉัย :</b><?= $diag->icd10 . ' ' . $diag->diag->icd10name;  ?><br>
<b>แพทย์ผู้ตรวจรักษา : </b> <?= $provider ?> <b>เลขที่ใบประกอบวิชาชีพ : </b><?= $lcno ?><br>
<b>สถานะการจำหน่าย : </b> <?= $visit->an == 0 ? $visit->dc->nameovstos : $ipt->dctype->namedchtyp; ?> <b>โรงพยาบาลที่ส่งต่อ : </b> <?= $visit->refer->referhosp->namehosp ?>
<br>
<table border=1 cellspace="0" width="100%" style="border-collapse:collapse">
    <tr>
        <th>ลำดับ</th>
        <th width="350px">รายการ</th>
        <th>ค่าใช้จ่าย (บาท)</th>
        <th>หมายเหตุ</th>
    </tr>
    <tr>
        <td style="text-align:center">1</td>
        <td>ค่าห้อง/ค่าอาหาร</td>
        <td style="text-align:right"><?= number_format($c[16] + $c[17], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">2</td>
        <td>อวัยวะเทียมและอุปกรณ์ในการบำบัดรักษาโรค</td>
        <td style="text-align:right"><?= number_format($c[14], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">3</td>
        <td>ค่ายาและสารน้ำที่ใช้ในโรงพยาบาล</td>
        <td style="text-align:right"><?= number_format($c[8] + $c[9] + $c[10], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">4</td>
        <td>ค่ายากลับบ้าน</td>
        <td style="text-align:right"><?= number_format($c[13], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">5</td>
        <td>ค่าเวชภัณฑ์มิใช่ยา</td>
        <td style="text-align:right"><?= number_format($c[11], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">6</td>
        <td>ค่าบริการโลหิตและส่วนประกอบของโลหิต</td>
        <td style="text-align:right"><?= number_format($c[12], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">7</td>
        <td>ค่าตรวจทางห้องปฏิบัติการ</td>
        <td style="text-align:right"><?= number_format($c[1], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">8</td>
        <td>ค่าตรวจวินิจฉัยและรักษาทางรังสีวิทยา</td>
        <td style="text-align:right"><?= number_format($c[2], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">9</td>
        <td>ค่าตรวจพิเศษอื่น ๆ</td>
        <td style="text-align:right"><?= number_format($c[3], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">10</td>
        <td>ค่าอุปกรณ์ของใช้และเครื่องมือทางการแพทย์</td>
        <td style="text-align:right"><?= number_format($c[15], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">11</td>
        <td>ค่าทำหัตถการและวิสัญญี</td>
        <td style="text-align:right"><?= number_format($c[4], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">12</td>
        <td>ค่าบริการทางการพยาบาล</td>
        <td style="text-align:right"><?= number_format($c[5], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">13</td>
        <td>ค่าบริการทางทันตกรรม</td>
        <td style="text-align:right"><?= number_format($c[19], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">14</td>
        <td>ค่าบริการทางกายภาพบำบัด</td>
        <td style="text-align:right"><?= number_format($c[6], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">15</td>
        <td>ค่าแพทย์ทางเลือก/ค่าบริการแพทย์แผนไทย</td>
        <td style="text-align:right"><?= number_format($c[7] + $c[22], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">16</td>
        <td>ค่าธรรมเนียมทางบุคคลากรทางการแพทย์</td>
        <td style="text-align:right"><?= number_format($c[21], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">17</td>
        <td>ค่าบริการพาหนะขนส่ง</td>
        <td style="text-align:right"><?= number_format($c[23], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td style="text-align:center">18</td>
        <td>ค่าบริการอื่นๆ</td>
        <td style="text-align:right"><?= number_format($c[18] + $c[20], 2) ?></td>
        <td>&emsp;</td>
    </tr>
    <tr>
        <td colspan=2 style="text-align:center">รวม</td>
        <td style="text-align:right"><?= number_format($total, 2) ?></td>
        <td>&emsp;</td>
    </tr>
</table>
<div align="center">
    รวมเป็นเงินทั้งสิ้น <?= number_format($total, 2) ?> (<?= num2wordsThai(number_format($total, 2)) ?>)
</div>
<br><br>
<table width="100%">
    <tr>
        <td style="text-align:center">(นางสาวศิริรัตน์ มาตขาว)</td>
        <td style="text-align:center">(นางสุรภารัชต์ ทองหิน)</td>
    </tr>
    <tr>
        <td style="text-align:center">นักวิชาการสาธารณสุข</td>
        <td style="text-align:center">พยาบาลวิชาชีพชำนาญการ</td>
    </tr>
    <tr>
        <td style="text-align:center">ผู้รายงาน</td>
        <td style="text-align:center">หัวหน้ากลุ่มงานประกันสุขภาพ ยุทธศาสตร์ และสารสนเทศทางการแพทย์</td>
    </tr>
    <tr>
        <td style="text-align:center"> </td>
        <td style="text-align:center">_____ / ______ / 25___</td>
    </tr>
</table>
<?php
if ($visit->pttype == '36') {
?>
    </pagebreak>
    <h4 align="center">แบบฟอร์มรายงานผู้ประกันตนเข้ารับการรักษา<br>
        โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี 34000 <br>
        โทรศัพท์ 045-304205 ต่อ 122,124 โทรสาร 045-304206 E-mail: lsk.hospital@gmail.com</h4>
    <b>เรื่อง </b>ขอแจ้งให้โรงพยาบาลตามบัตรรับรองสิมธิของผู้ประกันตน<br>
    <b>เรียน </b>งานประกันสังคม<?= $insure->getHospName() ?><br>
    <b>ขอแจ้งชื่อผู้ประกันตน ชื่อ-สกุล </b><?= $pt->getFullName() ?> <b> เลขบัตรประจำตัวประชาชน </b><?= $pt->pop_id ?>
    <b>เข้ารับการรักษาวันที่ </b><?= $visit->getVisitDate() ?><br>
    <b>อาการแรกรับ </b>
    <?php
    $pi = Pillness::find()->where(['vn' => $visit->vn])->all();
    foreach ($pi as $r) {
        echo $r->pillness;
    }
    ?>
    <br>
    <b>การวินิจฉัยเบื้องต้น </b><?= $diag->icd10 . ' ' . $diag->diag->icd10name;  ?><br>
    <b>น้ำหนัก </b><?= $visit->bw ?> ก.ก. <b>ส่วนสูง </b><?= $visit->height ?> ซ.ม. <b>อุณหภูมิ </b><?= $visit->tt ?> c <b>ชีพจร </b><?= $visit->pr ?>/min
    <b>ความดัน </b><?= $visit->sbp . '/' . $visit->dbp ?> mmHg <b>อัตราการหายใจ </b><?= $visit->rr ?>/min <br>
    <b>ให้การรักษา </b>
    <?php
    $pe = Sign::find()->where(['vn' => $visit->vn])->all();
    foreach ($pe as $r) {
        echo $r->sign;
    }
    ?>
    <br>
    <b>แพทย์ผู้ตรวจรักษา : </b> <?= $provider ?> <b>เลขที่ใบประกอบวิชาชีพ : </b><?= $lcno ?><br>
    <b>หอผู้ป่วย : </b><?= $ipt->wardadmit->nameidpm ?> <b>เบอร์ติดต่อภายใน </b>045-304205 ต่อ 116 <br>
    <b> ชื่อผู้ประสานงาน </b>นางสุรภารัชต์ ทองหิน <b>เบอร์ติดต่อ </b>088-5669145 <b>E-mail: </b> lsk.hospital@gmail.com<br>
    จึงเรียนมาเพื่อโปรดทราบและพิจารณารับผิดชอบค่ารักษาพยาบาลดังกล่าวแก่ผู้ประกันตนด้วย <br><br>
    <table width="100%">
        <tr>
            <td width="50%"> &emsp;</td>
            <td align="center"> ขอแสดงความนับถือ<br><br><br>( <?= $provider ?> )<br><b>ว.</b><?= $lcno ?><br></td>
        </tr>
    </table>
    <h3 align="center">แบบฟอร์มตอบกลับความคุ้มครอง</h3>
    ชื่อโรงพยาบาล .............................................................................................................................................. <br>
    โทรศัพท์ .................................................................................. โทรสาร ........................................................ <br>
    เรื่อง ขอรับผิดชอบค่ารักษาพยาบาล<br>
    เรียน ผู้รับผิดชอบงานประกันสุขภาพ<br>
    รายละเอียด .........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................<br>
    ชื่อผู้ประสานงาน ................................................... โทรศัพท์ ................................ E-mail ...................................... <br>
<?php
}
