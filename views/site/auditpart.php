<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EditResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานผู้ป่วย '.$pt->fname.' '.$pt->lname;
?>
<div class="edit-result-index">
    <div class="row">
        <div class="col-md-3">
            <?= Html::a('หน้าแรก', ['index'], ['class' => 'btn btn-primary btn-lg','style'=> 'width:200px']) ?>
        </div>
        <div class="col-md-9">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>

    <br>
    
    <?php     
        for($i=1;$i <= $part;$i++){
    ?>
        <div class="col-md-2">
            <?= Html::a('ส่วนที่ '.$i,['auditpart','id'=>$pt->hn,'part'=>$i],['class' => 'btn btn-primary','target'=>'blank']) ?>
        </div>
    <?php 
    }
    ?>

    </div>
