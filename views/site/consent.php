<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EditResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'พิมพ์ หนังสือแสดงความนินยอมเปิดเผยข้อมูลด้านสุขภาพของบุคลทางอิเล็คทรอนิกส์';
?>
<div class="edit-result-index">

    <div class="row">
        <div class="col-md-3">
            <?= Html::a('หน้าแรก', ['index'], ['class' => 'btn btn-primary btn-lg', 'style' => 'width:200px']) ?>
        </div>
        <div class="col-md-9">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div>
        <?php echo $this->render('_search_consent', ['model' => $searchModel]); ?>
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'hn',
            'pop_id',
            [
                'label' => 'ชื่อ-สกุล',
                'value' => function ($model) {
                    return $model->getFullname();
                }
            ],
            [
                'label' => 'อายุ',
                'value' => function ($model) {
                    return $model->getAge();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['style' => 'width:120px;'],
                //'buttonOptions'=>['class'=>'btn btn-primary'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('พิมพ์', ['print-consent', 'id' => $model->hn], ['class' => 'btn btn-info', 'target' => 'blank']);
                    }
                ],
            ],
        ],
    ]); ?>
</div>