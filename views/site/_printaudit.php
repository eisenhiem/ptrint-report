<?php 
use app\models\Pttype;
use app\models\Sign;
use app\models\Lbbk;
use app\models\Labresult;
use app\models\Pillness;
use app\models\Ovstdx;
use app\models\Icd101;
use app\models\Oprt;
use app\models\Config;
use app\models\Pi;
use app\models\Pe;
use app\models\Symptm;
use app\models\Ph;
use app\models\Cln;
use app\models\Visitadvice;
use app\models\Prsc;
use app\models\Prscdt;
use app\models\Oapp;
use app\models\Ovstost;
use app\models\Orfro;
use app\models\Visitnoteconsult;
use app\models\Medusage;
use app\models\Hospcode;
use app\models\Xryrgt;
use app\models\Dct;
use app\models\Lab;
use app\models\Xray;
use app\models\Meditem;
use app\models\Lablabel;

$config = Config::find()->one();

function age($bdate,$vdate)
{
    $difference = $bdate->diff($vdate); 
    
    $age = $difference->format('%y');  
    return $age;
} 
  
?>
<h3 align="center">ใบนำส่งเอกสารเพื่อการตรวจสอบ</h3>
<div align="center">
โรงพยาบาล<?= $config->hi_hsp_nm ?> เวชระเบียนลำดับที่ ......... จาก จำนวน ......... ฉบับ<br>
HN <?= $pt->hn ?> เวชระเบียนฉบับนี้มีเอกสารจำนวน ......... แผ่น<br>
</div>
<table border=1 cellspace="0" width="100%" style="border-collapse:collapse"> 
    <tr>
        <th width = "65%">รายการ</th>
        <th>จำนวนเอกสาร(แผ่น)</th>
        <th>หมายเหตุ</th>
    </tr>
    <tr>
        <td>
        &emsp;1 . ใบบันทึกการรักษาผู้ป่วยนอก(OPD card)
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
        &emsp;2 . ใบสั่งยา
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
        &emsp;3 . ผลการตรวจทางห้องปฏิบัติการที่สำคัญต่อการวินิจฉัยโรค
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
        &emsp;4 . ผลการตรวจรังสีวิทยา, ผลพยาธิวิทยา และผลการตรวพิเศษอื่นๆ
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>
        &emsp;5. เอกสารอื่นๆที่เกี่ยวข้อง<br>
            ระบุ.............................................................................
        </td>
        <td></td>
        <td></td>
    </tr>
</table>
&emsp;ลงชื่อ <img class="card-img-top" src=<?= Yii::$app->request->baseUrl.'/images/sender.jpg'?> style="width:120px;"> (ผู้จัดเตรียม/ส่งเอกสาร) &emsp;&emsp; ลงชื่อ <img class="card-img-top" src=<?= Yii::$app->request->baseUrl.'/images/coder.jpg'?> style="width:120px;"> (Coder)<br>
&emsp;&emsp;&emsp;( นางสุรภารัชต์ ทองหิน )&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;( นายจักรพงษ์ วงศ์กมลาไสย )<br>

&emsp;หมายเลขโทรศัพท์ที่สามารถติดต่อได้ 045-304-205 ต่อ 124
<pagebreak>
<?php 
$i=0;
foreach($visit as $v)
{
    $birthdate = new DateTime($ptinfo->birth);
    $visit_date = new DateTime($v->vstdttm);
    $i++;

    $cc = Symptm::find()->where(['vn' =>$v->vn])->one();
    $pi = Pi::findOne($v->vn);
    $pe = Pe::findOne($v->vn);
    $ph = Ph::findOne($v->vn);
    $advice = Visitadvice::find()->where(['vn' => $v->vn])->one();
    $diag = Ovstdx::find()->where(['vn' => $v->vn])->orderBy('id')->all();
    $proc = Oprt::find()->where(['vn' => $v->vn])->orderBy('id')->all();
    $lab = Lbbk::find()->where(['vn' => $v->vn])->all();
    $xry = Xryrgt::find()->where(['vn' => $v->vn])->all();
    $appoint = Oapp::find()->where(['vn' => $v->vn])->all();
    $out = Ovstost::find()->where(['ovstost' => $v->ovstost])->one();
    $refer = Orfro::find()->where(['vn' => $v->vn])->one();
    $consult = Visitnoteconsult::find()->where(['vn' => $v->vn])->one();

?>
    <h2 align="center">ประวัติการรักษาผู้ป่วยนอก OPD CARD</h2>
    <h3 align="center">โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี</h3>
<p>
   ชื่อ : <?php echo $ptinfo->prename.$ptinfo->ptname; ?> เพศ :<?php echo $ptinfo->sex;?> 
   อายุ : <?php echo age($birthdate,$visit_date); ?> ปี HN :<?php echo $pt->hn; ?> เลขบัตรประชาชน :<?php echo $ptinfo->cid;?><br>
   สิทธิ์ : <?php echo $v->type->namepttype; ?>&emsp; 
   รับบริการวันที่ <?= $v->getVisitDate() ?>&emsp;แผนก <?= $v->clinic->namecln; ?>
</p>
<p>
&emsp;<b>V/S </b>
 BT: <?= $v->tt ?> ºC, BP: <?= $v->sbp.'/'.$v->dbp ?> mmHg, PR: <?= $v->pr ?> /m, RR: <?= $v->rr ?> /m,
  BW: <?= $v->bw ?> kg, ส่วนสูง: <?= $v->height?> cm, BMI:<?php if($v->height <> 0) { echo round(($v->bw)/(($v->height/100)*($v->height/100)),2);} ?> 
</p>
<p>
&emsp;<b>Chief Compaint :</b> <?php if($cc){echo $cc->symptom;} ?><br>
&emsp;<b>Present Illness :</b> <?php if($pi){echo $pi->pi;} ?><br>      
&emsp;<b>Past History : </b><?php if($ph){echo $ph->ph;} ?><br>    
&emsp;<b>Laboratory :</b> 
    <?php
      foreach($lab as $r){
        $l = Lab::findOne($r->labcode);
        echo $l->labname.',';
      }
    ?><br>
&emsp;</b>X-Ray : </b> 
      <?php
        foreach($xry as $r){
          $l = Xray::findOne($r->xrycode);
          echo $l->xryname.',';
        }
      ?><br>    
&emsp;<b>Physical Exam :</b>      
      <?php if($pe){echo $pe->pe;} ?><br>    
&emsp;<b>Diagnosis :</b><br>
      <?php 
        foreach($diag as $dx){
            $diag_name = Icd101::find()->where(['icd10'=>$dx->icd10])->one();
            echo '&emsp;&emsp;&emsp;&emsp;'.$dx->icd10.'=>'.$diag_name->icd10name."<br>";
        }
      ?>
&emsp;<b>Procedure : </b> 
    <?php
      foreach($proc as $pr){
        echo $pr->icd9cm.'=>'.$pr->icd9name.',';
      }
    ?><br>
&emsp;<b>Consult : </b><?php if($consult){echo $consult->detail;} ?><br>
&emsp;<b>นัด : </b>    
    <?php
      foreach($appoint as $r){
        $c = Cln::find()->where('substr(cln,1,1) = :cln',[':cln' => substr($r->cln,0,1)])->one();
        echo $c->namecln.':'.$r->dscrptn.' วันที่ '.$r->getFudate().'<br>';
      }
    ?><br>
&emsp;<b>คำแนะนำ : </b><?php if($advice){echo $advice->advice;} ?><br>
&emsp;<b>สถานะจำหน่าย : </b> 
    <?php 
      if($v->ovstost == 1){
        echo "กลับบ้าน";
      } else if($v->ovstost == 2){
        echo "เสียชีวิต";
      } else if($v->ovstost == 4){
        echo "รับรักษาในโรงพยาบาล AN:" . $v->an;
      } else if($v->ovstost == 3){
        $h = Hospcode::find()->where(['off_id' =>$refer->rfrlct])->one();
      echo "ส่งต่อ :".$h->namehosp;
      }
    ?><br>
&emsp;<b>แพทย์ผู้รักษา: </b> 
    <?php 
      if(strlen($v->dct) == 5 ){
        $doctor = Dct::find()->where(['lcno' => $v->dct])->one();
      } else {
        $doctor = Dct::find()->where(['dct' => substr($v->dct,2,2)])->one();
      }
      echo $doctor->fname.' '. $doctor->lname .'<b> เลขที่ใบประกอบวิชาชีพ:</b> '.$doctor->lcno;
    ?>
</p>
<pagebreak>
<?php 
    }
foreach($visit as $vr)
{
    $birthdate = new DateTime($ptinfo->birth);
    $serv_date = new DateTime($vr->vstdttm);

    $prsc = Prsc::find()->where(['vn' => $vr->vn])->one();
    if($prsc){
      $rx = Prscdt::find()->where(['prscno' => $prsc->prscno])->all();    
?>
    <h2 align="center">ใบสั่งยาผู้ป่วยนอก เลขที่ <?= $prsc->prscno ?></h2>
    <h3 align="center">โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี</h3>
    ชื่อ : <?php echo $ptinfo->prename.$ptinfo->ptname; ?> เพศ :<?php echo $ptinfo->sex;?> 
    อายุ : <?php echo age($birthdate,$serv_date); ?> ปี HN :<?php echo $pt->hn; ?> เลขบัตรประชาชน :<?php echo $ptinfo->cid;?><br>
    สิทธิ์ : <?php echo $vr->type->namepttype; ?>&emsp; 
    รับบริการวันที่ <?= $vr->getVisitDate() ?>&emsp;แผนก <?= $vr->clinic->namecln; ?><br>
    <table border=1 cellspace="0" width="100%" style="border-collapse:collapse"> 
    <tr>
      <th width=40%><b>ชื่อยา</b></th>
      <th width=40%><b>วิธีใช้</b></th>
      <th width=20%><b>จำนวน</b></th>
    </tr>

<?php
    

    foreach($rx as $r){
        $unit = Meditem::findOne($r->meditem);
        $use = Medusage::find()->where(['dosecode' => $r->medusage])->one();
        if($use){
          echo '<tr><td>'.$r->nameprscdt.'</td><td>'. $use->doseprn1.' '.$use->doseprn2.'</td><td>'.$r->qty.' '.$unit->pres_unt.'</td></tr>';
        } else {
          echo '<tr><td>'.$r->nameprscdt.'</td><td> ไม่ระบุ </td><td>'.$r->qty.' '.$unit->pres_unt.'</td></tr>';
        }
       }
?>
    </table>
<pagebreak>
<?php 
    }
} 
foreach($visit as $vl)
{
    $lab = Lbbk::find()->where(['vn' => $vl->vn])->all();
    if($lab){
        echo '<h2 align="center">ผลการตรวจทางห้องปฏิบัติการ</h2>';
        echo 'ชื่อ : ' . $ptinfo->prename.$ptinfo->ptname.' เพศ : '. $ptinfo->sex .' อายุ : '. age($birthdate,$serv_date) .' ปี HN : ' . $pt->hn .' เลขบัตรประชาชน : '. $ptinfo->cid .'<br>';
        // echo 'สิทธิ์ : ' . $vr->type->namepttype ,' &emsp;รับบริการวันที่ '. $vr->getVisitDate() .'&emsp;แผนก '.$vr->clinic->namecln .'<br>';
    foreach($lab as $l){
        $labs = Labresult::find()->where(['ln'=>$l->ln])->all();
        echo '&emsp;<b>Lab Number:</b>'.$l->ln.' <b>รายการ</b>'.$l->lab->labname.' <b>วันที่สั่งตรวจ '.$l->getServdate().'<br>';
        echo '<table border=1 cellspace="0" width="100%" style="border-collapse:collapse">';
        echo '<tr><th width="20%">Test</th><th width="20%">Result</th><th  width="10%">unit</th><th width="30%">Referance</th><th width="20%">หมายเหตุ</th></tr>';
        foreach($labs as $result){
            $lab_name = Lablabel::find()->where(['labcode' => $l->labcode,'fieldname' =>strtoupper($result->lab_code_local)])->one();
            echo '<tr><td>'.$lab_name->fieldlabel.'</td><td>'.$result->labresult.'</td><td>'.$result->unit.'</td><td>'.$result->normal.'</td><td>'.$result->comment.'</td></tr>';
        }
        echo '</table>';
    }
    echo '<pagebreak>';
    }
} 
?>