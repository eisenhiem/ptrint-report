<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Print Report';
?>
<div class="site-index">

  <div class="body-content">
    <?php
    if (Yii::$app->user->isGuest) {
    ?>
      <div class="jumbotron">
        <h1>ระบบรายงานเอกสารจากระบบ HIS</h1>
        <p class="lead"><?= Yii::$app->user->isGuest ? 'กรุณา Log In เพื่อเข้าใช้งาน' : 'เลือก Ward เพื่อเข้าใช้งาน' ?></p>
      <?php
      echo Html::a('เข้าสู่ระบบ', ['/user/security/login'], ['class' => 'btn btn-primary btn-lg', 'style' => 'width:300px']);
      echo "</div>";
    } else {
      ?>
      <div class="row">
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/sss72.png' ?> style="width:200px;height:200px" alt="ประกันสังคม">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารค่าใช้จ่ายสิทธิ์ประกันสังคมที่มารับบริการที่โรงพยาบาล 72 ชั่วโมง</p>
              <?= Html::a('เอกสาร 72 ชั่วโมง', ['sssother'], ['class' => 'btn btn-primary btn-lg stretched-link', 'style' => 'width:200px'],) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/sss.png' ?> style="width:200px;height:200px" alt="รายวัน">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารค่าใช้จ่ายผู้ป่วยสิทธิ์ประกันสังคมทุพพลภาพ</p>
              <?= Html::a('ค่าใช้จ่ายรายวัน', ['ssi'], ['class' => 'btn btn-warning btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/sss.png' ?> style="width:200px;height:200px" alt="ประกันสังคม">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารค่าใช้จ่ายสิทธิ์ประกันสังคมผู้ป่วยใน รพ.สรรพสิทธิประสงค์</p>
              <?= Html::a('เอกสาร สรรพสิทธิ์ฯ', ['sss'], ['class' => 'btn btn-success btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/sss.png' ?> style="width:200px;height:200px" alt="รายวัน">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารค่าใช้จ่ายผู้ป่วยใน<br>รายละเอียดแยกรายวัน</p>
              <?= Html::a('ค่าใช้จ่ายรายวัน', ['daily'], ['class' => 'btn btn-warning btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/accident.png' ?> style="width:200px;height:200px" alt="อุบัติเหตุจราจร">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารประกอบการเบิกผู้ป่วย<br>เคส พรบ.จราจรทางบก</p>
              <?= Html::a('เอกสารประกอบ พรบ.', ['prb'], ['class' => 'btn btn-warning btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/audit.png' ?> style="width:200px;height:200px" alt="เอกสาร Audit">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารประกอบการ<br>Audit เวชระเบียน</p>
              <?= Html::a('เอกสารประกอบ Audit.', ['audit'], ['class' => 'btn btn-info btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/certificate.png' ?> style="width:200px;height:200px" alt="เอกสารใบรับรองแพทย์">
            </div>
            <div class="card-body">
              <p class="card-text">พิมพ์เอกสารแสดงความยินยอมเปิดเผยข้อมูลด้านสุขภาพของบุคล</p>
              <?= Html::a('เอกสาร Consent Form', ['consent'], ['class' => 'btn btn-info btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-3">
          <div class="card text-center" style="border:0">
            <div style="text-align:center">
              <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/certificate.png' ?> style="width:200px;height:200px" alt="รายการขอ authen code">
            </div>
            <div class="card-body">
              <p class="card-text">บันทึกการขอ Authen Code <br> และรายการขอ Authen</p>
              <?= Html::a('Authen Code', ['authen/index'], ['class' => 'btn btn-info btn-lg stretched-link', 'style' => 'width:200px']) ?>
            </div>
          </div>
        </div>
        <?php
        if (Yii::$app->user->identity->isAdmin) {
        ?>
          <div class="col-md-6 col-xl-3">
            <div class="card text-center" style="border:0">
              <div style="text-align:center">
                <img class="card-img-top" src=<?= Yii::$app->request->baseUrl . '/images/config.png' ?> style="width:200px;height:200px" alt="ตั้งค่าผู้ใช้ระบบ">
              </div>
              <div class="card-body">
                <p class="card-text">ตั้งค่าผู้ใช้งาน ระบบพิมพ์เอกสารต่างๆ จากโปรแกรม HI</p>
                <?= Html::a('ตั้งค่าผู้ใช้งานระบบ', ['/user/admin/index'], ['class' => 'btn btn-danger btn-lg stretched-link', 'style' => 'width:200px']) ?>
              </div>
            </div>
          </div>

      <?php
        }
      }
      ?>
      </div>
  </div>
  </div>
</div>