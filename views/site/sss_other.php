<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EditResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานผู้ป่วยสิทธิ์ประกันสังคม รพ.อื่น';
?>
<div class="edit-result-index">

<div class="row">
        <div class="col-md-3">
            <?= Html::a('หน้าแรก', ['index'], ['class' => 'btn btn-primary btn-lg','style'=> 'width:200px']) ?>
        </div>
        <div class="col-md-9">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
    </div>
    <div>
        <?php echo $this->render('_searchsss72', ['model' => $searchModel]); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                //s'vstdttm',
                [
                    'label' => 'วันที่มารับบริการ',
                    'value' => function ($model) {
                        return $model->getVisitdate();
                    }
                ],
                'hn',
                'an',
                [
                    'label' => 'ชื่อ-สกุล',
                    'value' => function ($model) {
                        return $model->pt ? $model->pt->getFullname():'';
                    }
                ],
                [
                    'label' => 'อายุ',
                    'value' => function ($model) {
                        return $model->pt ? $model->pt->getAge():'';
                    }
                ],
                [
                    'label' => 'สิทธิ์การรักษา',
                    'value' => function ($model) {
                        return $model->type->getTypename();
                    }
                ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                //'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['sssprint','id'=>$model->vn],['class' => 'btn btn-info','target'=>'blank']);
                    }
                ]
             ],
        ],
    ]); ?>
</div>
