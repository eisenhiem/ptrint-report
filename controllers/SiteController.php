<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ovst;
use app\models\OvstSearch;
use app\models\Incoth;
use app\models\Pt;
use app\models\Ovstdx;
use app\models\Prsc;
use app\models\Ipt;
use app\models\Iptdx;
use app\models\Insure;
use app\models\Dct;
use app\models\Dentist;
use app\models\Lbbk;
use app\models\Oprt;
use app\models\Xryrgt;
use app\models\PtSearch;
use app\models\Ptinfo;
use kartik\mpdf\Pdf;
use yii\debug\models\timeline\DataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSss()
    {
        $searchModel = new OvstSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filter get all SSS
        $dataProvider->query->andFilterWhere(['pttype' => '34'])->andFilterWhere(['!=','an', '0']);
        if(!$params["OvstSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['vn'=>SORT_DESC]);
        
        return $this->render('sss', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionDaily()
    {
        $searchModel = new OvstSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filter get all SSS
        $dataProvider->query->andFilterWhere(['!=','an', '0']);
        if(!$params["OvstSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['an'=>SORT_DESC]);
        
        return $this->render('daily', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionAudit()
    {
        $searchModel = new PtSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(!$params["PtSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // filter get all SSS 10669
        // $dataProvider->query->andFilterWhere(['pttype' => '34']);
        
        // Sort By HN order
        $dataProvider->query->orderBy(['hn'=>SORT_ASC]);
        
        return $this->render('audit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionSssother()
    {
        $searchModel = new OvstSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filter get all SSS other
        $dataProvider->query->andFilterWhere(['pttype' => '36']);
        if(!$params["OvstSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['vn'=>SORT_DESC]);
        
        return $this->render('sss_other', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionVisit($id)
    {
        $searchModel = new OvstSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['hn' => $id]);
        
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['vn'=>SORT_DESC]);
        
        return $this->render('visit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' =>$id,
        ]);
    } 

    public function actionConsent()
    {
        $searchModel = new PtSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(!$params["PtSearch"]){
            $dataProvider->query->where(['hn'=> -1 ]);
        }
        
        return $this->render('consent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionPrb()
    {
        $searchModel = new OvstSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filter get all สิทธิ์ พรบ.
        $dataProvider->query->andFilterWhere(['in','pttype',['12','35']]);
        if(!$params["OvstSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['vn'=>SORT_DESC]);
        
        return $this->render('prb', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionSsi()
    {
        $searchModel = new OvstSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // filter get all สิทธิ์ พรบ.
        $dataProvider->query->andFilterWhere(['in','pttype',['38']]);
        if(!$params["OvstSearch"]){
            $dataProvider->query->where(['hn'=> -1]);
        }
        // Sort By vn Descrease order
        $dataProvider->query->orderBy(['vn'=>SORT_DESC]);
        
        return $this->render('ssi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    public function actionSssprint($id)
    {
        $model = Incoth::find()->where(['vn'=>$id])->all();
        $visit = Ovst::findOne($id);
        $pt = Pt::findOne($visit->hn);
        if($visit->an == 0) {
            $diag = Ovstdx::find()->where(['vn'=>$id,'cnt' => 1])->One();
            if($visit->cln == '40100'){
                $provider = $visit->dent->provider->getProviderName();
                $lcno = $visit->dent->provider->lcno;
            }else {
                if (strlen($visit->dct) == 5 ) {
                    $provider = $visit->doctor->getProviderName();
                    $lcno = $visit->dct;
                } else {
                    $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                    $provider = $doctor->getProviderName();
                    $lcno = $doctor->lcno;
                }
            }
        } else {
            $diag = Iptdx::find()->where(['an'=>$visit->an,'itemno'=>1])->One();
            if (strlen($diag->dct) == 5 ) {
                $provider = $diag->doctor->getProviderName();
                $lcno = $diag->dct;
            } else {
                $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                $provider = $doctor->getProviderName();
                $lcno = $doctor->lcno;
            }

        }
        $drug = Prsc::find()->where(['vn'=>$id])->all();
        $ipt = Ipt::find()->where(['vn' => $id])->One();
        $insure = Insure::find()->where(['hn'=>$visit->hn,'pttype'=>$visit->pttype])->One();

        $content = $this->renderPartial('_printsss',[
            'model' => $model,
            'pt' => $pt,
            'visit' => $visit,
            'diag' => $diag,
            'drug' => $drug,
            'ipt' => $ipt,
            'insure' => $insure,
            'provider' => $provider,
            'lcno' => $lcno,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionPrbprint($id)
    {
        $model = Incoth::find()->where(['vn'=>$id])->all();
        $visit = Ovst::findOne($id);
        $pt = Pt::findOne($visit->hn);
        if($visit->an == 0) {
            $diag = Ovstdx::find()->where(['vn'=>$id,'cnt' => 1])->One();
            if($visit->cln == '40100'){
                $provider = $visit->dent->provider->getProviderName();
                $lcno = $visit->dent->provider->lcno;
            }else {
                if (strlen($visit->dct) == 5 ) {
                    $provider = $visit->doctor->getProviderName();
                    $lcno = $visit->dct;
                } else {
                    $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                    $provider = $doctor->getProviderName();
                    $lcno = $doctor->lcno;
                }
            }
        } else {
            $diag = Iptdx::find()->where(['an'=>$visit->an,'itemno'=>1])->One();
            if (strlen($diag->dct) == 5 ) {
                $provider = $diag->doctor->getProviderName();
                $lcno = $diag->dct;
            } else {
                $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                $provider = $doctor->getProviderName();
                $lcno = $doctor->lcno;
            }

        }
        $drug = Prsc::find()->where(['vn'=>$id])->all();
        $lab = Lbbk::find()->where(['vn'=>$id])->all();
        $proc = Oprt::find()->where(['vn'=>$id])->all();
        $xray = Xryrgt::find()->where(['vn'=>$id])->all();
        $ipt = Ipt::find()->where(['vn' => $id])->One();
        $insure = Insure::find()->where(['hn'=>$visit->hn,'pttype'=>$visit->pttype])->One();

        $content = $this->renderPartial('_printprb',[
            'model' => $model,
            'pt' => $pt,
            'visit' => $visit,
            'diag' => $diag,
            'drug' => $drug,
            'lab' => $lab,
            'proc' => $proc,
            'xray' => $xray,
            'ipt' => $ipt,
            'insure' => $insure,
            'provider' => $provider,
            'lcno' => $lcno,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionAuditpart($id,$part)
    {
        $date = date('Y');
        $sdate = $date-1;
        $date = $date.'-12-31 23:59:59';
        $sdate = $sdate.'-01-01 00:00:00';
        $ptinfo = Ptinfo::findOne($id);
        $pt = Pt::findOne($id);
        $offset = (($part - 1 ) * 20 ) +1 ;

        $visit = Ovst::find()->where(['hn'=>$id])->andWhere(['between','vstdttm',$sdate,$date])->orderBy(['vn'=>SORT_DESC])->limit(20)->offset($offset)->all();

        $content = $this->renderPartial('_printaudit',[
            'pt' => $pt,
            'visit' => $visit,
            'ptinfo' => $ptinfo,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Audit : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionAuditprint($id)
    {
        $date = date('Y');
        $sdate = $date-1;
        $date = $date.'-12-31 23:59:59';
        $sdate = $sdate.'-01-01 00:00:00';
        $ptinfo = Ptinfo::findOne($id);
        $pt = Pt::findOne($id);
        $count = Ovst::find()->where(['hn'=>$id])->andWhere(['between','vstdttm',$sdate,$date])->count();

        if($count > 20) {
            $part = ceil($count/20);
            
            return $this->render('auditpart',[
                'pt' => $pt,
                'part' => $part,
            ]);
        }
        $visit = Ovst::find()->where(['hn'=>$id])->andWhere(['between','vstdttm',$sdate,$date])->orderBy(['vn'=>SORT_DESC])->all();

        $content = $this->renderPartial('_printaudit',[
            'pt' => $pt,
            'visit' => $visit,
            'ptinfo' => $ptinfo,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Audit : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionPrintConsent($id)
    {
        $model = Pt::find()->where(['hn'=>$id])->one();

        $content = $this->renderPartial('_print_consent',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionPrintDaily($id)
    {
        $visit = Ovst::find()->where(['an' => $id])->one();
        $model = Incoth::find()->where(['vn'=>$visit->vn])->groupBy('date')->all();
        $pt = Pt::findOne($visit->hn);
        if($visit->an == 0) {
            $diag = Ovstdx::find()->where(['vn'=>$id,'cnt' => 1])->One();
            if($visit->cln == '40100'){
                $provider = $visit->dent->provider->getProviderName();
                $lcno = $visit->dent->provider->lcno;
            } else {
                if (strlen($visit->dct) == 5 ) {
                    $provider = $visit->doctor->getProviderName();
                    $lcno = $visit->dct;
                } else {
                    $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                    $provider = $doctor->getProviderName();
                    $lcno = $doctor->lcno;
                }
            }
        } else {
            $diag = Iptdx::find()->where(['an'=>$visit->an,'itemno'=>1])->One();
            if (strlen($diag->dct) == 5 ) {
                $provider = $diag->doctor->getProviderName();
                $lcno = $diag->dct;
            } else {
                $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                $provider = $doctor->getProviderName();
                $lcno = $doctor->lcno;
            }

        }
        $drug = Prsc::find()->where(['vn'=>$visit->vn])->all();
        $lab = Lbbk::find()->where(['vn'=>$visit->vn])->all();
        $proc = Oprt::find()->where(['vn'=>$visit->vn])->all();
        $xray = Xryrgt::find()->where(['vn'=>$visit->vn])->all();
        $ipt = Ipt::find()->where(['vn' => $visit->vn])->One();
        $insure = Insure::find()->where(['hn'=>$visit->hn,'pttype'=>$visit->pttype])->One();

        $content = $this->renderPartial('_print_daily',[
            'model' => $model,
            'pt' => $pt,
            'visit' => $visit,
            'diag' => $diag,
            'drug' => $drug,
            'lab' => $lab,
            'proc' => $proc,
            'xray' => $xray,
            'ipt' => $ipt,
            'insure' => $insure,
            'provider' => $provider,
            'lcno' => $lcno,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'AN : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionPrintSsi($id)
    {
        $visit = Ovst::find()->where(['vn' => $id])->one();
        $model = Incoth::find()->where(['vn'=>$visit->vn])->groupBy('date')->all();
        $pt = Pt::findOne($visit->hn);
        if($visit->an == 0) {
            $diag = Ovstdx::find()->where(['vn'=>$id,'cnt' => 1])->One();
            if($visit->cln == '40100'){
                $provider = $visit->dent->provider->getProviderName();
                $lcno = $visit->dent->provider->lcno;
            } else {
                if (strlen($visit->dct) == 5 ) {
                    $provider = $visit->doctor->getProviderName();
                    $lcno = $visit->dct;
                } else {
                    $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                    $provider = $doctor->getProviderName();
                    $lcno = $doctor->lcno;
                }
            }
        } else {
            $diag = Iptdx::find()->where(['an'=>$visit->an,'itemno'=>1])->One();
            if (strlen($diag->dct) == 5 ) {
                $provider = $diag->doctor->getProviderName();
                $lcno = $diag->dct;
            } else {
                $doctor = Dct::find()->where(['dct'=> substr($visit->dct,2,2)])->one();
                $provider = $doctor->getProviderName();
                $lcno = $doctor->lcno;
            }

        }
        $drug = Prsc::find()->where(['vn'=>$visit->vn])->all();
        $lab = Lbbk::find()->where(['vn'=>$visit->vn])->all();
        $proc = Oprt::find()->where(['vn'=>$visit->vn])->all();
        $xray = Xryrgt::find()->where(['vn'=>$visit->vn])->all();
        $ipt = Ipt::find()->where(['vn' => $visit->vn])->One();
        $insure = Insure::find()->where(['hn'=>$visit->hn,'pttype'=>$visit->pttype])->One();

        $content = $this->renderPartial('_print_daily',[
            'model' => $model,
            'pt' => $pt,
            'visit' => $visit,
            'diag' => $diag,
            'drug' => $drug,
            'lab' => $lab,
            'proc' => $proc,
            'xray' => $xray,
            'ipt' => $ipt,
            'insure' => $insure,
            'provider' => $provider,
            'lcno' => $lcno,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'แบบแสดงรายกาารค่าใช้จ่าย'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

}
