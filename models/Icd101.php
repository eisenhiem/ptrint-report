<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "icd101".
 *
 * @property string $icd10
 * @property string $accpdx
 * @property string $icd10name
 * @property string $who
 * @property string $i10tm
 * @property string $i10tmd
 * @property string $chk_icd
 * @property string $sex
 * @property string $ageduse
 * @property string $agemin
 * @property string $agemax
 * @property string $agedmin
 * @property string $chronic
 * @property string $group21
 * @property string $name_t
 * @property string $code_cat
 * @property string $spclty
 * @property string $icd_use
 * @property string $group_298_disease
 * @property string $rdu_uri
 * @property string $rdu_age
 * @property string $rdu_derm
 */
class Icd101 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icd101';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['icd10', 'accpdx', 'icd10name', 'who', 'i10tm', 'i10tmd', 'chk_icd', 'sex', 'ageduse', 'agemin', 'agemax', 'agedmin', 'chronic', 'group21', 'name_t', 'code_cat', 'spclty', 'group_298_disease', 'rdu_uri', 'rdu_age', 'rdu_derm'], 'required'],
            [['icd10', 'group21'], 'string', 'max' => 7],
            [['accpdx', 'icd_use', 'rdu_uri', 'rdu_age', 'rdu_derm'], 'string', 'max' => 1],
            [['icd10name', 'name_t'], 'string', 'max' => 90],
            [['who', 'i10tm', 'i10tmd', 'chk_icd', 'sex', 'chronic', 'code_cat', 'spclty'], 'string', 'max' => 2],
            [['ageduse', 'agemin', 'agemax', 'agedmin', 'group_298_disease'], 'string', 'max' => 3],
            [['icd10'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'icd10' => 'Icd10',
            'accpdx' => 'Accpdx',
            'icd10name' => 'Icd10name',
            'who' => 'Who',
            'i10tm' => 'I10tm',
            'i10tmd' => 'I10tmd',
            'chk_icd' => 'Chk Icd',
            'sex' => 'Sex',
            'ageduse' => 'Ageduse',
            'agemin' => 'Agemin',
            'agemax' => 'Agemax',
            'agedmin' => 'Agedmin',
            'chronic' => 'Chronic',
            'group21' => 'Group21',
            'name_t' => 'Name T',
            'code_cat' => 'Code Cat',
            'spclty' => 'Spclty',
            'icd_use' => 'Icd Use',
            'group_298_disease' => 'Group 298 Disease',
            'rdu_uri' => 'Rdu Uri',
            'rdu_age' => 'Rdu Age',
            'rdu_derm' => 'Rdu Derm',
        ];
    }
}
