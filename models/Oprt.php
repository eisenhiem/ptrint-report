<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oprt".
 *
 * @property int $vn
 * @property string $opdttm
 * @property int $an
 * @property string $icd9cm
 * @property string $icd9name
 * @property string|null $dct
 * @property int $orno
 * @property int $charge
 * @property string $oppnote
 * @property int $rcptno
 * @property string $codeicd9id
 * @property int|null $qty
 * @property int $id
 */
class Oprt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oprt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'icd9cm', 'icd9name', 'orno', 'charge', 'oppnote', 'rcptno', 'codeicd9id'], 'required'],
            [['vn', 'an', 'orno', 'charge', 'rcptno', 'qty'], 'integer'],
            [['opdttm'], 'safe'],
            [['oppnote'], 'string'],
            [['icd9cm', 'codeicd9id'], 'string', 'max' => 7],
            [['icd9name'], 'string', 'max' => 90],
            [['dct'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'opdttm' => 'Opdttm',
            'an' => 'An',
            'icd9cm' => 'Icd9cm',
            'icd9name' => 'Icd9name',
            'dct' => 'Dct',
            'orno' => 'Orno',
            'charge' => 'Charge',
            'oppnote' => 'Oppnote',
            'rcptno' => 'Rcptno',
            'codeicd9id' => 'Codeicd9id',
            'qty' => 'Qty',
            'id' => 'ID',
        ];
    }

    public function getProc()
    {
        return $this->hasOne(Prcd::className(),['id' => 'codeicd9id']);
    }

}
