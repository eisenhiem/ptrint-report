<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dchtype".
 *
 * @property string $namedchtyp
 * @property int $dchtype
 */
class Dchtype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dchtype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namedchtyp', 'dchtype'], 'required'],
            [['dchtype'], 'integer'],
            [['namedchtyp'], 'string', 'max' => 40],
            [['dchtype'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namedchtyp' => 'Namedchtyp',
            'dchtype' => 'Dchtype',
        ];
    }
}
