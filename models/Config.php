<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setup".
 *
 * @property int $idset
 * @property string $nameset
 * @property string $hi_hsp_nm
 * @property string $hi_chw_cd
 * @property string $hi_chw_nm
 * @property string $hi_amp_cd
 * @property string $hi_hsp_cd
 * @property string $timeprint
 * @property string $host
 * @property string $_editpass
 * @property string $hi2epidem
 * @property int $_prnono_ph
 * @property int $_prnono_er
 * @property int $_prnono_sp
 * @property int $_prnono_md
 * @property int $_prnono_dr
 * @property int $_prnono_dt
 * @property int $_prnono_lb
 * @property int $_prnono_dc
 * @property int $_prnono_rc
 * @property int $_prnono_ht
 * @property int $_prnono_tb
 * @property int $_lprinto
 * @property string $_drg
 * @property int $_dct_chgi
 * @property int $_dct_chgo
 * @property int $_brr_chg
 * @property int $_fuday
 * @property int $_pth
 * @property int $_hdspc_pth
 * @property int $_hdspc_xry
 * @property int $_hdspc_rec
 * @property int $_hdspc_phm
 * @property int $_lngth_xry
 * @property int $_lngth_phm
 * @property string $_psswrd
 * @property string $_admtime
 * @property string $_odac
 * @property string $_odpc
 * @property string $_bidac
 * @property string $_bidpc
 * @property string $_tidac
 * @property string $_hs
 * @property string $_tidpc
 * @property string $_qidac
 * @property string $_qidpc
 * @property string $_q2
 * @property string $_q4
 * @property string $_q6
 * @property string $_q8
 * @property string $_q12
 * @property string $passdx
 * @property string $ptted
 * @property string $passset
 * @property string $blockdx
 * @property string $hcode
 * @property string $xrypass
 * @property string $hostxry
 * @property string $userxry
 * @property string $pacstype
 * @property int $_dtt_chgo
 * @property int $_dtt_chgi
 * @property string $blocklab
 * @property int $fixdlogi
 * @property int $htype
 * @property string $addr
 * @property string $tel
 * @property string $fontchar
 * @property string $fontcolf
 * @property string $fontcolb
 * @property string $referhosp
 * @property int $_ptho
 * @property string $internet_serv
 * @property string $lockday
 * @property string $setmedprt
 * @property int $chgrefer
 * @property string $pacsdtt
 * @property string $hosprefer
 * @property string $setfindtx
 * @property string $_prnono_sp_pcu
 * @property string $fixdrughiv
 * @property string $serverpic
 * @property string $userpic
 * @property string $passpic
 * @property string $prtnavi
 * @property string $chksitdrfee
 * @property string $chksitcgd
 * @property string $mdrgentxt
 * @property string $serverapi
 * @property string $prt_q4u
 * @property string $qtyprtq
 * @property string $fixlabnotshw
 * @property string $printerid
 * @property string $smallqueue
 * @property string $_prnono_ttm
 * @property string|null $chkinsmed
 * @property string|null $ptright
 * @property int|null $_d_m_chgo
 * @property int|null $_d_m_chgi
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nameset', 'hi_hsp_nm', 'hi_chw_cd', 'hi_chw_nm', 'hi_amp_cd', 'hi_hsp_cd', 'timeprint', 'host', '_editpass', '_prnono_rc', '_prnono_ht', '_prnono_tb', '_drg', '_dct_chgi', '_dct_chgo', '_brr_chg', '_fuday', '_pth', '_hdspc_pth', '_hdspc_xry', '_hdspc_rec', '_hdspc_phm', '_lngth_xry', '_lngth_phm', '_psswrd', '_admtime', '_odac', '_odpc', '_bidac', '_bidpc', '_tidac', '_hs', '_tidpc', '_qidac', '_qidpc', '_q2', '_q4', '_q6', '_q8', '_q12', 'passdx', 'ptted', 'passset', 'blockdx', 'hcode', 'xrypass', 'hostxry', 'userxry', 'pacstype', '_dtt_chgo', '_dtt_chgi', 'blocklab', 'fixdlogi', 'htype', 'addr', 'tel', 'fontchar', 'fontcolf', 'fontcolb', 'referhosp', '_ptho', 'internet_serv', 'lockday', 'chgrefer', 'pacsdtt', 'hosprefer', 'setfindtx', '_prnono_sp_pcu', 'fixdrughiv', 'serverpic', 'userpic', 'passpic', 'chksitdrfee', 'chksitcgd', 'mdrgentxt', 'serverapi', 'prt_q4u', 'qtyprtq', 'fixlabnotshw', 'printerid', 'smallqueue', '_prnono_ttm'], 'required'],
            [['hi2epidem'], 'safe'],
            [['_prnono_ph', '_prnono_er', '_prnono_sp', '_prnono_md', '_prnono_dr', '_prnono_dt', '_prnono_lb', '_prnono_dc', '_prnono_rc', '_prnono_ht', '_prnono_tb', '_lprinto', '_dct_chgi', '_dct_chgo', '_brr_chg', '_fuday', '_pth', '_hdspc_pth', '_hdspc_xry', '_hdspc_rec', '_hdspc_phm', '_lngth_xry', '_lngth_phm', '_dtt_chgo', '_dtt_chgi', 'fixdlogi', 'htype', '_ptho', 'chgrefer', '_d_m_chgo', '_d_m_chgi'], 'integer'],
            [['nameset', '_bidac', '_bidpc', '_q12'], 'string', 'max' => 10],
            [['hi_hsp_nm', 'hi_chw_nm', '_q4'], 'string', 'max' => 30],
            [['hi_chw_cd', 'hi_amp_cd', 'pacstype'], 'string', 'max' => 2],
            [['hi_hsp_cd', '_psswrd'], 'string', 'max' => 8],
            [['timeprint', 'ptted', 'fontchar', 'internet_serv'], 'string', 'max' => 50],
            [['host', '_qidac', '_qidpc', '_q6', 'userxry', 'fontcolf', 'fontcolb'], 'string', 'max' => 20],
            [['_editpass'], 'string', 'max' => 16],
            [['_drg', 'hcode', 'hosprefer'], 'string', 'max' => 5],
            [['_admtime', '_odac', '_odpc', '_hs'], 'string', 'max' => 4],
            [['_tidac', '_tidpc', '_q8'], 'string', 'max' => 15],
            [['_q2'], 'string', 'max' => 60],
            [['passdx', 'passset', 'xrypass', 'printerid'], 'string', 'max' => 9],
            [['blockdx'], 'string', 'max' => 500],
            [['hostxry', 'userpic', 'passpic', 'chksitdrfee', 'chksitcgd'], 'string', 'max' => 40],
            [['blocklab', 'fixlabnotshw', 'ptright'], 'string', 'max' => 200],
            [['addr', 'tel', 'referhosp', 'fixdrughiv', 'serverpic', 'serverapi'], 'string', 'max' => 100],
            [['lockday'], 'string', 'max' => 3],
            [['setmedprt', 'pacsdtt', 'setfindtx', '_prnono_sp_pcu', 'prtnavi', 'mdrgentxt', 'prt_q4u', 'qtyprtq', 'smallqueue', '_prnono_ttm', 'chkinsmed'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idset' => 'Idset',
            'nameset' => 'Nameset',
            'hi_hsp_nm' => 'Hi Hsp Nm',
            'hi_chw_cd' => 'Hi Chw Cd',
            'hi_chw_nm' => 'Hi Chw Nm',
            'hi_amp_cd' => 'Hi Amp Cd',
            'hi_hsp_cd' => 'Hi Hsp Cd',
            'timeprint' => 'Timeprint',
            'host' => 'Host',
            '_editpass' => 'Editpass',
            'hi2epidem' => 'Hi2epidem',
            '_prnono_ph' => 'Prnono Ph',
            '_prnono_er' => 'Prnono Er',
            '_prnono_sp' => 'Prnono Sp',
            '_prnono_md' => 'Prnono Md',
            '_prnono_dr' => 'Prnono Dr',
            '_prnono_dt' => 'Prnono Dt',
            '_prnono_lb' => 'Prnono Lb',
            '_prnono_dc' => 'Prnono Dc',
            '_prnono_rc' => 'Prnono Rc',
            '_prnono_ht' => 'Prnono Ht',
            '_prnono_tb' => 'Prnono Tb',
            '_lprinto' => 'Lprinto',
            '_drg' => 'Drg',
            '_dct_chgi' => 'Dct Chgi',
            '_dct_chgo' => 'Dct Chgo',
            '_brr_chg' => 'Brr Chg',
            '_fuday' => 'Fuday',
            '_pth' => 'Pth',
            '_hdspc_pth' => 'Hdspc Pth',
            '_hdspc_xry' => 'Hdspc Xry',
            '_hdspc_rec' => 'Hdspc Rec',
            '_hdspc_phm' => 'Hdspc Phm',
            '_lngth_xry' => 'Lngth Xry',
            '_lngth_phm' => 'Lngth Phm',
            '_psswrd' => 'Psswrd',
            '_admtime' => 'Admtime',
            '_odac' => 'Odac',
            '_odpc' => 'Odpc',
            '_bidac' => 'Bidac',
            '_bidpc' => 'Bidpc',
            '_tidac' => 'Tidac',
            '_hs' => 'Hs',
            '_tidpc' => 'Tidpc',
            '_qidac' => 'Qidac',
            '_qidpc' => 'Qidpc',
            '_q2' => 'Q2',
            '_q4' => 'Q4',
            '_q6' => 'Q6',
            '_q8' => 'Q8',
            '_q12' => 'Q12',
            'passdx' => 'Passdx',
            'ptted' => 'Ptted',
            'passset' => 'Passset',
            'blockdx' => 'Blockdx',
            'hcode' => 'Hcode',
            'xrypass' => 'Xrypass',
            'hostxry' => 'Hostxry',
            'userxry' => 'Userxry',
            'pacstype' => 'Pacstype',
            '_dtt_chgo' => 'Dtt Chgo',
            '_dtt_chgi' => 'Dtt Chgi',
            'blocklab' => 'Blocklab',
            'fixdlogi' => 'Fixdlogi',
            'htype' => 'Htype',
            'addr' => 'Addr',
            'tel' => 'Tel',
            'fontchar' => 'Fontchar',
            'fontcolf' => 'Fontcolf',
            'fontcolb' => 'Fontcolb',
            'referhosp' => 'Referhosp',
            '_ptho' => 'Ptho',
            'internet_serv' => 'Internet Serv',
            'lockday' => 'Lockday',
            'setmedprt' => 'Setmedprt',
            'chgrefer' => 'Chgrefer',
            'pacsdtt' => 'Pacsdtt',
            'hosprefer' => 'Hosprefer',
            'setfindtx' => 'Setfindtx',
            '_prnono_sp_pcu' => 'Prnono Sp Pcu',
            'fixdrughiv' => 'Fixdrughiv',
            'serverpic' => 'Serverpic',
            'userpic' => 'Userpic',
            'passpic' => 'Passpic',
            'prtnavi' => 'Prtnavi',
            'chksitdrfee' => 'Chksitdrfee',
            'chksitcgd' => 'Chksitcgd',
            'mdrgentxt' => 'Mdrgentxt',
            'serverapi' => 'Serverapi',
            'prt_q4u' => 'Prt Q4u',
            'qtyprtq' => 'Qtyprtq',
            'fixlabnotshw' => 'Fixlabnotshw',
            'printerid' => 'Printerid',
            'smallqueue' => 'Smallqueue',
            '_prnono_ttm' => 'Prnono Ttm',
            'chkinsmed' => 'Chkinsmed',
            'ptright' => 'Ptright',
            '_d_m_chgo' => 'D M Chgo',
            '_d_m_chgi' => 'D M Chgi',
        ];
    }
}
