<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "incoth".
 *
 * @property int $vn
 * @property int $an
 * @property string $date
 * @property int $time
 * @property string $income
 * @property string $pttype
 * @property string $paidst
 * @property int $rcptno
 * @property float $rcptamt
 * @property int $recno
 * @property string $cgd
 * @property string $codecheck
 * @property int $ln
 * @property int $id
 */
class Incoth extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'incoth';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'time', 'income', 'pttype', 'paidst', 'rcptno', 'rcptamt', 'recno', 'cgd', 'codecheck', 'ln'], 'required'],
            [['vn', 'an', 'time', 'rcptno', 'recno', 'ln'], 'integer'],
            [['date'], 'safe'],
            [['rcptamt'], 'number'],
            [['income', 'pttype'], 'string', 'max' => 2],
            [['paidst'], 'string', 'max' => 1],
            [['cgd'], 'string', 'max' => 5],
            [['codecheck'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'an' => 'An',
            'date' => 'Date',
            'time' => 'Time',
            'income' => 'หมวดค่าใช้จ่าย',
            'pttype' => 'สิทธิ์',
            'paidst' => 'Paidst',
            'rcptno' => 'เลขที่ใบเสร็จ',
            'rcptamt' => 'ค่าใช้จ่าย',
            'recno' => 'Recno',
            'cgd' => 'รหัสกรมบัญชีกลาง',
            'codecheck' => 'Codecheck',
            'ln' => 'Ln',
            'id' => 'ID',
        ];
    }

    public function getInc()
    {
        return $this->hasOne(Income::className(),['costcenter' => 'income']);
    }

    public function getDate($date)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($date);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $result = $d.' '.$m.' '.$y;
        return $result;
    }
}
