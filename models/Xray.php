<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "xray".
 *
 * @property string $xrycode
 * @property string|null $xryname
 * @property int $unit
 * @property int $film_xray
 * @property int $pricexry
 * @property int $pricexrycgd
 * @property string $ptright
 * @property string $cgd
 * @property string $etype
 * @property string $chkshow
 * @property string $remark
 * @property string|null $chkchg
 * @property string|null $icd9cm
 */
class Xray extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xray';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['xrycode', 'unit', 'film_xray', 'pricexry', 'pricexrycgd', 'ptright', 'cgd', 'etype', 'remark'], 'required'],
            [['unit', 'film_xray', 'pricexry', 'pricexrycgd'], 'integer'],
            [['remark'], 'string'],
            [['xrycode'], 'string', 'max' => 4],
            [['xryname'], 'string', 'max' => 20],
            [['ptright'], 'string', 'max' => 200],
            [['cgd'], 'string', 'max' => 5],
            [['etype'], 'string', 'max' => 100],
            [['chkshow', 'chkchg'], 'string', 'max' => 1],
            [['icd9cm'], 'string', 'max' => 7],
            [['xrycode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'xrycode' => 'Xrycode',
            'xryname' => 'Xryname',
            'unit' => 'Unit',
            'film_xray' => 'Film Xray',
            'pricexry' => 'Pricexry',
            'pricexrycgd' => 'Pricexrycgd',
            'ptright' => 'Ptright',
            'cgd' => 'Cgd',
            'etype' => 'Etype',
            'chkshow' => 'Chkshow',
            'remark' => 'Remark',
            'chkchg' => 'Chkchg',
            'icd9cm' => 'Icd9cm',
        ];
    }
}
