<?php

namespace app\models;

use Yii;
use DateTime;
/**
 * This is the model class for table "lbbk".
 *
 * @property int $ln
 * @property string $lfudate
 * @property string $labcode
 * @property int $vn
 * @property string $reportby
 * @property string $requestby
 * @property int $fufinish
 * @property string $vstdttm
 * @property int $hn
 * @property int $an
 * @property string $senddate
 * @property int $sendtime
 * @property string $srvdate
 * @property int $srvtime
 * @property string $pttype
 * @property int $finish
 * @property string $c2automate
 * @property string $hcode
 * @property string $approve
 * @property string $approvedate
 * @property string $approvetime
 * @property string $lcomment
 * @property string $labcomment Comment of lab
 * @property string $labgroup
 * @property string $pdffile
 * @property string $accept
 * @property string $acceptby
 * @property string $accepttime
 */
class Lbbk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lbbk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lfudate', 'vstdttm', 'senddate', 'srvdate', 'approvedate'], 'safe'],
            [['labcode', 'vn', 'hn', 'an', 'sendtime', 'srvtime', 'pttype', 'c2automate', 'hcode', 'lcomment', 'labcomment', 'labgroup', 'acceptby', 'accepttime'], 'required'],
            [['vn', 'fufinish', 'hn', 'an', 'sendtime', 'srvtime', 'finish'], 'integer'],
            [['labcode'], 'string', 'max' => 3],
            [['reportby', 'requestby', 'approve'], 'string', 'max' => 100],
            [['pttype', 'labgroup'], 'string', 'max' => 2],
            [['c2automate', 'lcomment', 'accept'], 'string', 'max' => 1],
            [['hcode'], 'string', 'max' => 5],
            [['approvetime', 'accepttime'], 'string', 'max' => 4],
            [['labcomment'], 'string', 'max' => 255],
            [['pdffile', 'acceptby'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Ln',
            'lfudate' => 'Lfudate',
            'labcode' => 'Labcode',
            'vn' => 'Vn',
            'reportby' => 'Reportby',
            'requestby' => 'Requestby',
            'fufinish' => 'Fufinish',
            'vstdttm' => 'Vstdttm',
            'hn' => 'Hn',
            'an' => 'An',
            'senddate' => 'Senddate',
            'sendtime' => 'Sendtime',
            'srvdate' => 'Srvdate',
            'srvtime' => 'Srvtime',
            'pttype' => 'Pttype',
            'finish' => 'Finish',
            'c2automate' => 'C2automate',
            'hcode' => 'Hcode',
            'approve' => 'Approve',
            'approvedate' => 'Approvedate',
            'approvetime' => 'Approvetime',
            'lcomment' => 'Lcomment',
            'labcomment' => 'Labcomment',
            'labgroup' => 'Labgroup',
            'pdffile' => 'Pdffile',
            'accept' => 'Accept',
            'acceptby' => 'Acceptby',
            'accepttime' => 'Accepttime',
        ];
    }

    public function getLab()
    {
        return $this->hasOne(Lab::className(),['labcode' => 'labcode']);
    }

    public function getServdate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->vstdttm);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $h = $visit->format('H');
        $i = $visit->format('i');
        $result = $d.' '.$m.' '.$y.' เวลา '.$h.':'.$i.' น.';
        return $result;
    }

}
