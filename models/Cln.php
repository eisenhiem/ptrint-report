<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cln".
 *
 * @property int $id
 * @property string $namecln
 * @property string|null $costcenter
 * @property string $cln
 * @property string $dspname
 * @property string $odpm
 * @property string $costcente2
 * @property string $f21_55
 * @property string $fixdayfu
 * @property string $chronicfu
 * @property string $fixdaystp
 * @property string $sso_clinic
 * @property int $que_start_no
 * @property string $specialty
 */
class Cln extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cln';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namecln', 'dspname', 'odpm', 'costcente2', 'f21_55', 'fixdayfu', 'fixdaystp', 'sso_clinic', 'que_start_no', 'specialty'], 'required'],
            [['que_start_no'], 'integer'],
            [['namecln'], 'string', 'max' => 25],
            [['costcenter'], 'string', 'max' => 4],
            [['cln'], 'string', 'max' => 5],
            [['dspname', 'fixdaystp'], 'string', 'max' => 40],
            [['odpm', 'fixdayfu', 'sso_clinic'], 'string', 'max' => 2],
            [['costcente2', 'f21_55'], 'string', 'max' => 8],
            [['chronicfu'], 'string', 'max' => 1],
            [['specialty'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'namecln' => 'Namecln',
            'costcenter' => 'Costcenter',
            'cln' => 'Cln',
            'dspname' => 'Dspname',
            'odpm' => 'Odpm',
            'costcente2' => 'Costcente2',
            'f21_55' => 'F21 55',
            'fixdayfu' => 'Fixdayfu',
            'chronicfu' => 'Chronicfu',
            'fixdaystp' => 'Fixdaystp',
            'sso_clinic' => 'Sso Clinic',
            'que_start_no' => 'Que Start No',
            'specialty' => 'Specialty',
        ];
    }
}
