<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ovstdx".
 *
 * @property int $id
 * @property int $vn
 * @property string $icd10
 * @property string $icd10name
 * @property int $cnt
 * @property int $consultid
 */
class Ovstdx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ovstdx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'icd10', 'icd10name', 'consultid'], 'required'],
            [['vn', 'cnt', 'consultid'], 'integer'],
            [['icd10'], 'string', 'max' => 7],
            [['icd10name'], 'string', 'max' => 90],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vn' => 'Vn',
            'icd10' => 'Icd10',
            'icd10name' => 'Icd10name',
            'cnt' => 'Cnt',
            'consultid' => 'Consultid',
        ];
    }

    public function getDiag()
    {
        return $this->hasOne(Icd101::className(),['icd10' => 'icd10']);
    }

}
