<?php

namespace app\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "ovst".
 *
 * @property int $vn
 * @property string $vstdttm
 * @property int $hn
 * @property string $cln
 * @property string $dct
 * @property string $pttype
 * @property string $sickdate
 * @property string $bw
 * @property string $height
 * @property string $tt
 * @property int $pr
 * @property int $rr
 * @property int $sbp
 * @property int $dbp
 * @property int $an
 * @property string $register
 * @property string $waist_cm
 */
class Ovst extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ovst';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vstdttm', 'sickdate'], 'safe'],
            [['hn', 'pttype', 'ovstist', 'ovstost', 'bw', 'height', 'tt', 'pr', 'rr', 'sbp', 'dbp', 'an', 'register', 'waist_cm'], 'required'],
            [['hn', 'ovstost', 'pr', 'rr', 'sbp', 'dbp', 'an'], 'integer'],
            [['bw', 'height', 'tt', 'waist_cm'], 'number'],
            [['cln', 'dct'], 'string', 'max' => 5],
            [['pttype', 'register'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'vstdttm' => 'วันที่มารับบริการ',
            'hn' => 'HN',
            'cln' => 'Clinic',
            'dct' => 'Doctor',
            'pttype' => 'สิทธิ์',
            'sickdate' => 'วันที่เริ่มป่วย',
            'ovstost' => 'สถานะกลับบ้าน',
            'bw' => 'น้ำหนัก(kg)',
            'height' => 'ส่วนสูง(cm)',
            'tt' => 'Temp (C)',
            'pr' => 'อัตราการเต้นของหัวใจ',
            'rr' => 'อัตราการหายใจ',
            'sbp' => 'ความดัน Systolic',
            'dbp' => 'ความดัน Diasystolic',
            'an' => 'AN',
            'register' => 'ผู้ลงทะเบียน',
            'waist_cm' => 'รอบเอว (cm)',
        ];
    }

    public function getPt()
    {
        return $this->hasOne(Pt::className(),['hn' => 'hn']);
    }

    public function getType()
    {
        return $this->hasOne(Pttype::className(),['pttype' => 'pttype']);
    }

    public function getClinic()
    {
        return $this->hasOne(Cln::className(),['cln' => 'cln']);
    }

    public function getRefer()
    {
        return $this->hasOne(Orfro::className(),['vn' => 'vn']);
    }

    public function getLab()
    {
        return $this->hasOne(Lbbk::className(),['vn' => 'vn']);
    }

    public function getRx()
    {
        return $this->hasOne(Prsc::className(),['vn' => 'vn']);
    }
    
    public function getDent()
    {
        return $this->hasOne(Dt::className(),['vn' => 'vn']);
    }

    public function getDoctor()
    {
        return $this->hasOne(Dct::className(),['lcno' => 'dct']);
    }

    public function getDc()
    {   
        return $this->hasOne(Ovstost::className(),['ovstost' => 'ovstost']);
    }

    public function getVisitdate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->vstdttm);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $h = $visit->format('H');
        $i = $visit->format('i');
        $result = $d.' '.$m.' '.$y.' เวลา '.$h.':'.$i.' น.';
        return $result;
    }

}
