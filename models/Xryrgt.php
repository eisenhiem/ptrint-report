<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "xryrgt".
 *
 * @property int $xan
 * @property int $vn
 * @property int $an
 * @property int $hn
 * @property string $xn
 * @property string $vstdate
 * @property int $xrytime
 * @property int $vsttime
 * @property string $xrycode
 * @property string $staff
 * @property int $fdent
 * @property int $bdent
 * @property int $f1417
 * @property int $f1414
 * @property int $f1215
 * @property int $f1114
 * @property int $f1012
 * @property int $f0810
 * @property int $f0714
 * @property int $b1417
 * @property int $b1414
 * @property int $b1215
 * @property int $b1114
 * @property int $b1012
 * @property int $b0810
 * @property int $b0714
 * @property string $fread
 * @property int $charge
 * @property int $fu
 * @property string $crok
 * @property string $xrydate
 */
class Xryrgt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xryrgt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'xn', 'xrytime', 'vsttime', 'xrycode', 'staff', 'fdent', 'bdent', 'f1417', 'f1414', 'f1215', 'f1114', 'f1012', 'f0810', 'f0714', 'b1417', 'b1414', 'b1215', 'b1114', 'b1012', 'b0810', 'b0714', 'fread', 'charge', 'crok', 'xrydate'], 'required'],
            [['vn', 'an', 'hn', 'xrytime', 'vsttime', 'fdent', 'bdent', 'f1417', 'f1414', 'f1215', 'f1114', 'f1012', 'f0810', 'f0714', 'b1417', 'b1414', 'b1215', 'b1114', 'b1012', 'b0810', 'b0714', 'charge', 'fu'], 'integer'],
            [['vstdate', 'xrydate'], 'safe'],
            [['fread'], 'string'],
            [['xn'], 'string', 'max' => 8],
            [['xrycode'], 'string', 'max' => 4],
            [['staff'], 'string', 'max' => 2],
            [['crok'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'xan' => 'Xan',
            'vn' => 'Vn',
            'an' => 'An',
            'hn' => 'Hn',
            'xn' => 'Xn',
            'vstdate' => 'Vstdate',
            'xrytime' => 'Xrytime',
            'vsttime' => 'Vsttime',
            'xrycode' => 'Xrycode',
            'staff' => 'Staff',
            'fdent' => 'Fdent',
            'bdent' => 'Bdent',
            'f1417' => 'F1417',
            'f1414' => 'F1414',
            'f1215' => 'F1215',
            'f1114' => 'F1114',
            'f1012' => 'F1012',
            'f0810' => 'F0810',
            'f0714' => 'F0714',
            'b1417' => 'B1417',
            'b1414' => 'B1414',
            'b1215' => 'B1215',
            'b1114' => 'B1114',
            'b1012' => 'B1012',
            'b0810' => 'B0810',
            'b0714' => 'B0714',
            'fread' => 'Fread',
            'charge' => 'Charge',
            'fu' => 'Fu',
            'crok' => 'Crok',
            'xrydate' => 'Xrydate',
        ];
    }
    public function getXray()
    {
        return $this->hasOne(Xray::className(),['xrycode' => 'xrycode']);
    }

}

