<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dt".
 *
 * @property int $dn
 * @property int $vn
 * @property string $vstdttm
 * @property int $an
 * @property int $hn
 * @property string $dnt
 * @property string $pttype
 * @property string $grp
 * @property string $schl
 * @property int $rcptno
 * @property string $srvdttm
 * @property int $consultid
 */
class Dt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'dnt', 'pttype', 'grp', 'schl', 'rcptno', 'srvdttm', 'consultid'], 'required'],
            [['vn', 'an', 'hn', 'rcptno', 'consultid'], 'integer'],
            [['vstdttm', 'srvdttm'], 'safe'],
            [['dnt'], 'string', 'max' => 5],
            [['pttype', 'grp'], 'string', 'max' => 2],
            [['schl'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dn' => 'Dn',
            'vn' => 'Vn',
            'vstdttm' => 'Vstdttm',
            'an' => 'An',
            'hn' => 'Hn',
            'dnt' => 'Dnt',
            'pttype' => 'Pttype',
            'grp' => 'Grp',
            'schl' => 'Schl',
            'rcptno' => 'Rcptno',
            'srvdttm' => 'Srvdttm',
            'consultid' => 'Consultid',
        ];
    }

    public function getProvider()
    {
        return $this->hasOne(Dentist::className(),['codedtt' => 'dnt']);
    }

}
