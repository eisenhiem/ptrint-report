<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meditem".
 *
 * @property string $ort
 * @property string $meditem
 * @property string $name
 * @property string $pres_nm
 * @property string $pres_unt
 * @property int $ed
 * @property string $medtype
 * @property string $medunit
 * @property string $medform
 * @property string $medgrp
 * @property string $phmdir
 * @property int|null $defqty
 * @property int|null $maxqty
 * @property int $packqty
 * @property string $storage
 * @property string $medusage
 * @property float $cost
 * @property float $price
 * @property float $chrg_rat
 * @property int $pack_chg
 * @property float $salerate
 * @property float $maxrfnd
 * @property float $maxdscnt
 * @property string $aux_lbl1
 * @property string $aux_lbl2
 * @property string $pres_ono
 * @property int $pck_dsp
 * @property string $pregnancy
 * @property string $nu
 * @property string $gp
 * @property string $ob
 * @property string $su
 * @property string $md
 * @property string $pd
 * @property string $fm
 * @property string $ey
 * @property string $en
 * @property string $ur
 * @property string $ptright
 * @property int $highalert
 * @property string $mgperkg
 * @property int $maxdose
 * @property float $strength
 * @property string $stdcode
 * @property string $tallman
 * @property string $ph
 * @property string $dt
 * @property string $pt
 * @property string $jt
 * @property string $type
 * @property string $tmtype thai_Massage_Drug
 * @property string $etype
 * @property string $edtype
 * @property string $note
 * @property string $chknote
 * @property string $medallow
 * @property string $pharadd
 * @property string $pharupd
 * @property string $dateupd
 * @property string $medright
 * @property string $tmtid
 * @property string $specprep
 * @property string $updateflag
 * @property string $updatepric
 * @property string $tradename
 * @property string $productcat
 * @property string $allowtmt
 * @property string $chkstock
 * @property string $codeinst
 * @property float $price_uc
 * @property float $price_sc
 * @property float $price_ss
 * @property string $aux_lbl3
 * @property string $nameth
 * @property string $statusprt
 * @property string $codeed
 * @property string $pcutype
 * @property string $medclass
 * @property string $serialno
 * @property string|null $url_qrcode
 * @property string|null $drugstore
 * @property string|null $clnotallow
 * @property string|null $ptt2pay
 */
class Meditem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meditem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ort', 'meditem', 'name', 'pres_nm', 'pres_unt', 'medtype', 'medunit', 'medform', 'medgrp', 'phmdir', 'packqty', 'storage', 'medusage', 'cost', 'price', 'chrg_rat', 'pack_chg', 'salerate', 'maxrfnd', 'maxdscnt', 'aux_lbl1', 'aux_lbl2', 'pres_ono', 'pregnancy', 'nu', 'gp', 'ob', 'su', 'md', 'pd', 'fm', 'ey', 'en', 'ur', 'ptright', 'mgperkg', 'maxdose', 'strength', 'stdcode', 'tallman', 'ph', 'dt', 'pt', 'jt', 'type', 'etype', 'edtype', 'note', 'chknote', 'medallow', 'pharadd', 'pharupd', 'dateupd', 'medright', 'tmtid', 'specprep', 'updateflag', 'updatepric', 'tradename', 'productcat', 'codeinst', 'price_uc', 'price_sc', 'price_ss', 'aux_lbl3', 'nameth', 'codeed', 'medclass', 'serialno'], 'required'],
            [['ed', 'defqty', 'maxqty', 'packqty', 'pack_chg', 'pck_dsp', 'highalert', 'maxdose'], 'integer'],
            [['cost', 'price', 'chrg_rat', 'salerate', 'maxrfnd', 'maxdscnt', 'strength', 'price_uc', 'price_sc', 'price_ss'], 'number'],
            [['dateupd', 'updatepric'], 'safe'],
            [['url_qrcode'], 'string'],
            [['ort', 'pres_ono', 'pregnancy', 'nu', 'gp', 'ob', 'su', 'md', 'pd', 'fm', 'ey', 'en', 'ur', 'ph', 'dt', 'pt', 'jt', 'type', 'tmtype', 'edtype', 'chknote', 'medallow', 'updateflag', 'allowtmt', 'chkstock', 'statusprt', 'codeed', 'pcutype', 'drugstore'], 'string', 'max' => 1],
            [['meditem', 'pres_unt', 'medtype', 'medunit', 'medform', 'medgrp', 'storage'], 'string', 'max' => 7],
            [['name', 'pres_nm', 'tallman'], 'string', 'max' => 50],
            [['phmdir'], 'string', 'max' => 8],
            [['medusage', 'pharadd', 'pharupd'], 'string', 'max' => 5],
            [['aux_lbl1', 'aux_lbl2', 'aux_lbl3'], 'string', 'max' => 3],
            [['ptright', 'note', 'medright', 'tradename', 'serialno'], 'string', 'max' => 200],
            [['mgperkg'], 'string', 'max' => 15],
            [['stdcode', 'tmtid'], 'string', 'max' => 24],
            [['etype', 'medclass'], 'string', 'max' => 100],
            [['specprep'], 'string', 'max' => 4],
            [['productcat'], 'string', 'max' => 2],
            [['codeinst'], 'string', 'max' => 10],
            [['nameth'], 'string', 'max' => 60],
            [['clnotallow', 'ptt2pay'], 'string', 'max' => 255],
            [['meditem'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ort' => 'Ort',
            'meditem' => 'Meditem',
            'name' => 'Name',
            'pres_nm' => 'Pres Nm',
            'pres_unt' => 'Pres Unt',
            'ed' => 'Ed',
            'medtype' => 'Medtype',
            'medunit' => 'Medunit',
            'medform' => 'Medform',
            'medgrp' => 'Medgrp',
            'phmdir' => 'Phmdir',
            'defqty' => 'Defqty',
            'maxqty' => 'Maxqty',
            'packqty' => 'Packqty',
            'storage' => 'Storage',
            'medusage' => 'Medusage',
            'cost' => 'Cost',
            'price' => 'Price',
            'chrg_rat' => 'Chrg Rat',
            'pack_chg' => 'Pack Chg',
            'salerate' => 'Salerate',
            'maxrfnd' => 'Maxrfnd',
            'maxdscnt' => 'Maxdscnt',
            'aux_lbl1' => 'Aux Lbl1',
            'aux_lbl2' => 'Aux Lbl2',
            'pres_ono' => 'Pres Ono',
            'pck_dsp' => 'Pck Dsp',
            'pregnancy' => 'Pregnancy',
            'nu' => 'Nu',
            'gp' => 'Gp',
            'ob' => 'Ob',
            'su' => 'Su',
            'md' => 'Md',
            'pd' => 'Pd',
            'fm' => 'Fm',
            'ey' => 'Ey',
            'en' => 'En',
            'ur' => 'Ur',
            'ptright' => 'Ptright',
            'highalert' => 'Highalert',
            'mgperkg' => 'Mgperkg',
            'maxdose' => 'Maxdose',
            'strength' => 'Strength',
            'stdcode' => 'Stdcode',
            'tallman' => 'Tallman',
            'ph' => 'Ph',
            'dt' => 'Dt',
            'pt' => 'Pt',
            'jt' => 'Jt',
            'type' => 'Type',
            'tmtype' => 'Tmtype',
            'etype' => 'Etype',
            'edtype' => 'Edtype',
            'note' => 'Note',
            'chknote' => 'Chknote',
            'medallow' => 'Medallow',
            'pharadd' => 'Pharadd',
            'pharupd' => 'Pharupd',
            'dateupd' => 'Dateupd',
            'medright' => 'Medright',
            'tmtid' => 'Tmtid',
            'specprep' => 'Specprep',
            'updateflag' => 'Updateflag',
            'updatepric' => 'Updatepric',
            'tradename' => 'Tradename',
            'productcat' => 'Productcat',
            'allowtmt' => 'Allowtmt',
            'chkstock' => 'Chkstock',
            'codeinst' => 'Codeinst',
            'price_uc' => 'Price Uc',
            'price_sc' => 'Price Sc',
            'price_ss' => 'Price Ss',
            'aux_lbl3' => 'Aux Lbl3',
            'nameth' => 'Nameth',
            'statusprt' => 'Statusprt',
            'codeed' => 'Codeed',
            'pcutype' => 'Pcutype',
            'medclass' => 'Medclass',
            'serialno' => 'Serialno',
            'url_qrcode' => 'Url Qrcode',
            'drugstore' => 'Drugstore',
            'clnotallow' => 'Clnotallow',
            'ptt2pay' => 'Ptt2pay',
        ];
    }
}
