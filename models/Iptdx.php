<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "iptdx".
 *
 * @property int $an
 * @property int $itemno
 * @property string|null $dct
 * @property string|null $icd10
 * @property string $icd10name
 * @property string $spclty
 * @property int $id
 */
class Iptdx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'iptdx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'itemno', 'icd10name', 'spclty'], 'required'],
            [['an', 'itemno'], 'integer'],
            [['dct'], 'string', 'max' => 5],
            [['icd10'], 'string', 'max' => 7],
            [['icd10name'], 'string', 'max' => 90],
            [['spclty'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'itemno' => 'Itemno',
            'dct' => 'Dct',
            'icd10' => 'Icd10',
            'icd10name' => 'Icd10name',
            'spclty' => 'Spclty',
            'id' => 'ID',
        ];
    }
    
    public function getDiag()
    {
        return $this->hasOne(Icd101::className(),['icd10' => 'icd10']);
    }

    public function getDoctor()
    {
        return $this->hasOne(Dct::className(),['lcno' => 'dct']);
    }


}
