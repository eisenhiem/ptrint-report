<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pillness".
 *
 * @property string $pillness
 * @property int $vn
 * @property int $id
 */
class Pillness extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pillness';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pillness', 'vn'], 'required'],
            [['vn'], 'integer'],
            [['pillness'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pillness' => 'Pillness',
            'vn' => 'Vn',
            'id' => 'ID',
        ];
    }
}
