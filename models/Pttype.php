<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pttype".
 *
 * @property string $namepttype
 * @property string $rights
 * @property string $pttype
 * @property string $stdcode
 * @property string $pay
 * @property string $op56
 * @property string $inscl
 * @property string $chkshow
 * @property string $instypeold
 * @property string $ins_code
 * @property string $chg18
 * @property string $income_id
 * @property string $sss_export
 */
class Pttype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pttype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namepttype', 'pttype'], 'required'],
            [['namepttype'], 'string', 'max' => 40],
            [['pttype', 'stdcode' ], 'string', 'max' => 2],
            [['inscl'], 'string', 'max' => 3],
            [['pttype'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namepttype' => 'สิทธิ์ที่มารับบริการ',
            'pttype' => 'รหัสสิทธิ์',
            'stdcode' => 'Stdcode',
            'inscl' => 'ประเภทสิทธิ์',
        ];
    }

    public function getTypename()
    {
        return '('.$this->pttype.') '.$this->namepttype;
    }

}
