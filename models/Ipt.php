<?php

namespace app\models;

use Yii;
use DateTime;
/**
 * This is the model class for table "ipt".
 *
 * @property int $vn
 * @property int $an
 * @property int $hn
 * @property string $ward
 * @property string $rgtdate
 * @property int $rgttime
 * @property string $pttype
 * @property string $prediag
 * @property string $dchdate
 * @property int $daycnt
 * @property int $dchtime
 * @property int $dchstts
 * @property int $dchtype
 * @property string $dthdiagdct
 * @property string $ipdlct
 * @property float|null $bmi
 * @property float $bw
 * @property float|null $height
 * @property float|null $tt
 * @property int $rr
 * @property int $pr
 * @property int $sbp
 * @property int $dbp
 * @property string $ipttype
 * @property int $painscore
 * @property string|null $symptmi
 * @property string|null $drg
 * @property float|null $rw
 * @property float|null $adjrw
 * @property string|null $error
 * @property string|null $warning
 * @property int|null $actlos
 * @property string|null $grouper_version
 * @property float|null $wtlos
 * @property int|null $ot
 * @property string|null $dept
 */
class Ipt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ipt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'rgttime', 'pttype', 'prediag', 'daycnt', 'dchtime', 'dchstts', 'dchtype', 'dthdiagdct', 'ipdlct', 'bw', 'rr', 'pr', 'sbp', 'dbp', 'ipttype', 'painscore'], 'required'],
            [['vn', 'an', 'hn', 'rgttime', 'daycnt', 'dchtime', 'dchstts', 'dchtype', 'rr', 'pr', 'sbp', 'dbp', 'painscore', 'actlos', 'ot'], 'integer'],
            [['rgtdate', 'dchdate'], 'safe'],
            [['bmi', 'bw', 'height', 'tt', 'rw', 'adjrw', 'wtlos'], 'number'],
            [['ward', 'pttype', 'ipttype', 'error', 'dept'], 'string', 'max' => 2],
            [['prediag'], 'string', 'max' => 7],
            [['dthdiagdct', 'ipdlct', 'drg'], 'string', 'max' => 5],
            [['symptmi'], 'string', 'max' => 1000],
            [['warning'], 'string', 'max' => 4],
            [['grouper_version'], 'string', 'max' => 20],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'an' => 'An',
            'hn' => 'Hn',
            'ward' => 'Ward',
            'rgtdate' => 'Rgtdate',
            'rgttime' => 'Rgttime',
            'pttype' => 'Pttype',
            'prediag' => 'Prediag',
            'dchdate' => 'Dchdate',
            'daycnt' => 'Daycnt',
            'dchtime' => 'Dchtime',
            'dchstts' => 'Dchstts',
            'dchtype' => 'Dchtype',
            'dthdiagdct' => 'Dthdiagdct',
            'ipdlct' => 'Ipdlct',
            'bmi' => 'Bmi',
            'bw' => 'Bw',
            'height' => 'Height',
            'tt' => 'Tt',
            'rr' => 'Rr',
            'pr' => 'Pr',
            'sbp' => 'Sbp',
            'dbp' => 'Dbp',
            'ipttype' => 'Ipttype',
            'painscore' => 'Painscore',
            'symptmi' => 'Symptmi',
            'drg' => 'Drg',
            'rw' => 'Rw',
            'adjrw' => 'Adjrw',
            'error' => 'Error',
            'warning' => 'Warning',
            'actlos' => 'Actlos',
            'grouper_version' => 'Grouper Version',
            'wtlos' => 'Wtlos',
            'ot' => 'Ot',
            'dept' => 'Dept',
        ];
    }

    public function getWardadmit()
    {
        return $this->hasOne(Idpm::className(),['idpm' => 'ward']);
    }

    public function getDctype()
    {
        return $this->hasOne(Dchtype::className(),['dchtype' => 'dchtype']);
    }
    
    public function getRefer()
    {
        return $this->hasOne(Orfro::className(),['vn' => 'vn']);
    }

    public function getDcDate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->dchdate);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        if(strlen($this->dchtime) == 4){
            $h = substr($this->dchtime,0,2);
            $i = substr($this->dchtime,2,2);
        } else {
            $h = '0'.substr($this->dchtime,0,1);
            $i = substr($this->dchtime,1,2);
        } 
        $result = $d.' '.$m.' '.$y.' เวลา '.$h.':'.$i.' น.';
        return $result;

    }

}
