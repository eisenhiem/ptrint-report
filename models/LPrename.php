<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_prename".
 *
 * @property string $prename_code
 * @property string|null $prename
 * @property string|null $prename_desc
 * @property string|null $int_code
 * @property string|null $sex
 * @property string|null $status
 */
class LPrename extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_prename';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prename_code'], 'required'],
            [['prename_code', 'int_code'], 'string', 'max' => 3],
            [['prename', 'prename_desc'], 'string', 'max' => 200],
            [['sex', 'status'], 'string', 'max' => 1],
            [['prename_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prename_code' => 'Prename Code',
            'prename' => 'Prename',
            'prename_desc' => 'Prename Desc',
            'int_code' => 'Int Code',
            'sex' => 'Sex',
            'status' => 'Status',
        ];
    }
}
