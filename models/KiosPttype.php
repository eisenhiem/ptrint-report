<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kios_pttype".
 *
 * @property int $id
 * @property string|null $cid
 * @property string|null $json_data
 * @property string|null $claimCode
 * @property string|null $claimType
 * @property string|null $cln
 * @property string|null $regist_date
 * @property string|null $regist_time
 */
class KiosPttype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kios_pttype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['json_data'], 'string'],
            [['regist_date', 'regist_time'], 'safe'],
            [['cid'], 'string', 'max' => 13],
            [['claimCode', 'claimType'], 'string', 'max' => 50],
            [['cln'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cid' => 'เลขบัตรประชาชน',
            'json_data' => 'Json Data',
            'claimCode' => 'Authen Code',
            'claimType' => 'ประเภท Authen',
            'cln' => 'Clinic',
            'regist_date' => 'Regist Date',
            'regist_time' => 'Regist Time',
        ];
    }

    public function getPt()
    {
        return $this->hasOne(Pt::className(),['pop_id' => 'cid']);
    }
}
