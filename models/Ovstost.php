<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ovstost".
 *
 * @property string $nameovstos
 * @property int $ovstost
 */
class Ovstost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ovstost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nameovstos', 'ovstost'], 'required'],
            [['ovstost'], 'integer'],
            [['nameovstos'], 'string', 'max' => 15],
            [['ovstost'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nameovstos' => 'Nameovstos',
            'ovstost' => 'Ovstost',
        ];
    }
}
