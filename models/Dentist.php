<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dentist".
 *
 * @property string $codedtt
 * @property string $pname
 * @property string $fname
 * @property string $lname
 * @property string $dspname
 * @property string $lcno
 * @property string $psswrd
 * @property string $registerno
 * @property string $council
 * @property string $sex
 * @property string|null $birth
 * @property string $providertype
 * @property string|null $startdate
 * @property string|null $outdate
 * @property string $movefrom
 * @property string $moveto
 * @property string $cid
 * @property string $dttype
 * @property string $statusdct
 * @property string $ldate
 */
class Dentist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dentist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codedtt', 'pname', 'fname', 'lname', 'dspname', 'lcno', 'psswrd', 'registerno', 'council', 'sex', 'providertype', 'movefrom', 'moveto', 'cid', 'dttype', 'ldate'], 'required'],
            [['birth', 'startdate', 'outdate', 'ldate'], 'safe'],
            [['codedtt', 'lcno', 'movefrom', 'moveto'], 'string', 'max' => 5],
            [['pname', 'registerno'], 'string', 'max' => 15],
            [['fname', 'lname'], 'string', 'max' => 25],
            [['dspname'], 'string', 'max' => 50],
            [['psswrd'], 'string', 'max' => 20],
            [['council', 'providertype'], 'string', 'max' => 2],
            [['sex', 'dttype', 'statusdct'], 'string', 'max' => 1],
            [['cid'], 'string', 'max' => 13],
            [['codedtt'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codedtt' => 'Codedtt',
            'pname' => 'Pname',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'dspname' => 'Dspname',
            'lcno' => 'Lcno',
            'psswrd' => 'Psswrd',
            'registerno' => 'Registerno',
            'council' => 'Council',
            'sex' => 'Sex',
            'birth' => 'Birth',
            'providertype' => 'Providertype',
            'startdate' => 'Startdate',
            'outdate' => 'Outdate',
            'movefrom' => 'Movefrom',
            'moveto' => 'Moveto',
            'cid' => 'Cid',
            'dttype' => 'Dttype',
            'statusdct' => 'Statusdct',
            'ldate' => 'Ldate',
        ];
    }

    public function getPrename()
    {
        return $this->hasOne(LPrename::className(),['prename_code' => 'pname']);
    }

    public function getProviderName()
    {
        return $this->prename->prename.$this->fname.' '.$this->lname;
    }


}
