<?php

namespace app\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "oapp".
 *
 * @property int $vn
 * @property string $vstdttm
 * @property int $an
 * @property int $hn
 * @property string $vstdate
 * @property int $vsttime
 * @property string $dct
 * @property string $cln
 * @property string $dscrptn
 * @property string $oappst
 * @property string $fudate
 * @property int $futime
 * @property int $fuok
 */
class Oapp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oapp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'vsttime', 'dscrptn', 'oappst', 'futime'], 'required'],
            [['vn', 'an', 'hn', 'vsttime', 'futime', 'fuok'], 'integer'],
            [['vstdttm', 'vstdate', 'fudate'], 'safe'],
            [['dct', 'cln'], 'string', 'max' => 5],
            [['dscrptn'], 'string', 'max' => 25],
            [['oappst'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'vstdttm' => 'Vstdttm',
            'an' => 'An',
            'hn' => 'Hn',
            'vstdate' => 'Vstdate',
            'vsttime' => 'Vsttime',
            'dct' => 'Dct',
            'cln' => 'Cln',
            'dscrptn' => 'Dscrptn',
            'oappst' => 'Oappst',
            'fudate' => 'Fudate',
            'futime' => 'Futime',
            'fuok' => 'Fuok',
        ];
    }

    public function getFudate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->fudate);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $result = $d.' '.$m.' '.$y ;
        return $result;
    }

}
