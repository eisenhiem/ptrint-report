<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "labresult".
 *
 * @property int $ln
 * @property string $labcode
 * @property string $lab_code_local
 * @property string|null $lab_name
 * @property string $labresult
 * @property string|null $unit
 * @property string|null $normal
 * @property string|null $comment
 */
class Labresult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labresult';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['labcode', 'lab_code_local', 'labresult'], 'required'],
            [['labresult', 'comment'], 'string'],
            [['labcode'], 'string', 'max' => 3],
            [['lab_code_local'], 'string', 'max' => 20],
            [['lab_name', 'unit', 'normal'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Ln',
            'labcode' => 'Labcode',
            'lab_code_local' => 'Lab Code Local',
            'lab_name' => 'Lab Name',
            'labresult' => 'Labresult',
            'unit' => 'Unit',
            'normal' => 'Normal',
            'comment' => 'Comment',
        ];
    }
    
}
