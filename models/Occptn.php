<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "occptn".
 *
 * @property string $nameoccptn
 * @property string $occptn
 */
class Occptn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'occptn';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nameoccptn', 'occptn'], 'required'],
            [['nameoccptn'], 'string', 'max' => 40],
            [['occptn'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nameoccptn' => 'Nameoccptn',
            'occptn' => 'Occptn',
        ];
    }
}
