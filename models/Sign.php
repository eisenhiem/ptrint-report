<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sign".
 *
 * @property int $vn
 * @property string $sign
 * @property int $an
 * @property int $id
 * @property int $consultid
 */
class Sign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'sign', 'an', 'consultid'], 'required'],
            [['vn', 'an', 'consultid'], 'integer'],
            [['sign'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'sign' => 'Sign',
            'an' => 'An',
            'id' => 'ID',
            'consultid' => 'Consultid',
        ];
    }
}
