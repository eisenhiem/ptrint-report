<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prsc".
 *
 * @property int $vn
 * @property int $prscno
 * @property string $prscdate
 * @property int $prsctime
 * @property int $an
 * @property string $pttype
 * @property string $paidst
 * @property string $charge
 * @property string $dscamt
 * @property int $rcptno
 * @property string $issprsn
 * @property string $sotpdate
 * @property string $sotptime
 * @property string $pharmacist
 * @property int $consultid
 * @property string $usehome
 */
class Prsc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prsc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'prsctime', 'an', 'pttype', 'paidst', 'charge', 'dscamt', 'rcptno', 'issprsn', 'sotpdate', 'sotptime', 'pharmacist', 'consultid'], 'required'],
            [['vn', 'prsctime', 'an', 'rcptno', 'consultid'], 'integer'],
            [['prscdate'], 'safe'],
            [['charge', 'dscamt'], 'number'],
            [['pttype', 'paidst'], 'string', 'max' => 2],
            [['issprsn'], 'string', 'max' => 5],
            [['sotpdate'], 'string', 'max' => 8],
            [['sotptime'], 'string', 'max' => 4],
            [['pharmacist'], 'string', 'max' => 6],
            [['usehome'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'prscno' => 'Prscno',
            'prscdate' => 'Prscdate',
            'prsctime' => 'Prsctime',
            'an' => 'An',
            'pttype' => 'Pttype',
            'paidst' => 'Paidst',
            'charge' => 'Charge',
            'dscamt' => 'Dscamt',
            'rcptno' => 'Rcptno',
            'issprsn' => 'Issprsn',
            'sotpdate' => 'Sotpdate',
            'sotptime' => 'Sotptime',
            'pharmacist' => 'Pharmacist',
            'consultid' => 'Consultid',
            'usehome' => 'Usehome',
        ];
    }

    public function getDrug()
    {
        return $this->hasMany(Prscdt::className(),['prscno' => 'prscno']);
    }

}
