<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KiosPttype;

/**
 * KiosPttypeSearch represents the model behind the search form of `app\models\KiosPttype`.
 */
class KiosPttypeSearch extends KiosPttype
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['cid', 'json_data', 'claimCode', 'claimType', 'cln', 'regist_date', 'regist_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KiosPttype::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'regist_date' => $this->regist_date,
            'regist_time' => $this->regist_time,
        ]);

        $query->andFilterWhere(['like', 'cid', $this->cid])
            ->andFilterWhere(['like', 'json_data', $this->json_data])
            ->andFilterWhere(['like', 'claimCode', $this->claimCode])
            ->andFilterWhere(['like', 'claimType', $this->claimType])
            ->andFilterWhere(['like', 'cln', $this->cln]);

        return $dataProvider;
    }
}
