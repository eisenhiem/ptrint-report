<?php

namespace app\models;

use Yii;
use DateTime;
/**
 * This is the model class for table "pt".
 *
 * @property int $hn
 * @property string $pname
 * @property string $fname
 * @property string $lname
 * @property string $brthdate
 * @property int $truebrth
 * @property string $dthdate
 * @property string $male
 * @property string $ctzshp
 * @property string $ntnlty
 * @property string $rlgn
 * @property string $occptn
 * @property string $bloodgrp
 * @property string $allergy
 * @property string $addrpart
 * @property string $moopart
 * @property string $tmbpart
 * @property string $amppart
 * @property string $chwpart
 * @property string $hometel
 * @property string $pttype
 * @property string $mrtlst
 * @property string $spsname
 * @property string $mthname
 * @property string $fthname
 * @property string $offaddr
 * @property string $offtel
 * @property string $infmname
 * @property string $prsnrlt
 * @property string $infmaddr
 * @property string $infmtel
 * @property string $opdlct
 * @property string $fdate
 * @property string $ldate
 * @property string $pop_id
 * @property string $height
 * @property string $register
 * @property string $pathimage
 * @property string $typearea
 * @property string $statusinfo
 * @property string $house_id
 * @property string $housetype
 * @property string $viptype
 * @property string $cidlabor
 * @property string $engfname
 * @property string $englname
 * @property string $engpname
 * @property string $familyno
 * @property string $father_cid
 * @property string $mother_cid
 * @property string $couple_cid
 * @property string $family_pos
 * @property string $movein
 * @property string $moveout
 * @property string $typedisch
 * @property string $labor
 * @property string $passport
 * @property string $vstatus
 * @property string $gencid
 * @property string $education
 * @property string $couple
 */
class Pt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pname', 'truebrth', 'male', 'ctzshp', 'ntnlty', 'rlgn', 'occptn', 'bloodgrp', 'allergy', 'addrpart', 'moopart', 'tmbpart', 'amppart', 'chwpart', 'hometel', 'pttype', 'mrtlst', 'spsname', 'mthname', 'fthname', 'offaddr', 'offtel', 'infmname', 'prsnrlt', 'infmaddr', 'infmtel', 'opdlct', 'pop_id', 'height', 'register', 'statusinfo', 'house_id', 'housetype', 'viptype', 'cidlabor', 'engfname', 'englname', 'engpname', 'father_cid', 'mother_cid', 'couple_cid', 'passport', 'vstatus', 'gencid', 'education', 'couple'], 'required'],
            [['brthdate', 'dthdate', 'fdate', 'ldate', 'movein', 'moveout'], 'safe'],
            [['truebrth'], 'integer'],
            [['height'], 'number'],
            [['typearea'], 'string'],
            [['pname'], 'string', 'max' => 15],
            [['fname', 'lname'], 'string', 'max' => 25],
            [['male', 'rlgn', 'mrtlst', 'housetype', 'family_pos', 'typedisch', 'vstatus'], 'string', 'max' => 1],
            [['ctzshp', 'ntnlty', 'bloodgrp', 'moopart', 'tmbpart', 'amppart', 'chwpart', 'pttype', 'register', 'viptype', 'labor', 'education'], 'string', 'max' => 2],
            [['occptn'], 'string', 'max' => 3],
            [['allergy'], 'string', 'max' => 60],
            [['addrpart', 'spsname', 'mthname', 'fthname', 'infmname'], 'string', 'max' => 30],
            [['hometel', 'offtel', 'infmtel', 'cidlabor', 'passport'], 'string', 'max' => 20],
            [['offaddr', 'infmaddr'], 'string', 'max' => 150],
            [['prsnrlt'], 'string', 'max' => 10],
            [['opdlct'], 'string', 'max' => 5],
            [['pop_id', 'familyno', 'father_cid', 'mother_cid', 'couple_cid', 'gencid'], 'string', 'max' => 13],
            [['pathimage', 'couple'], 'string', 'max' => 100],
            [['statusinfo', 'engfname', 'englname', 'engpname'], 'string', 'max' => 40],
            [['house_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'HN',
            'pname' => 'Pname',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'brthdate' => 'Brthdate',
            'truebrth' => 'Truebrth',
            'dthdate' => 'Dthdate',
            'male' => 'Male',
            'ctzshp' => 'Ctzshp',
            'ntnlty' => 'Ntnlty',
            'rlgn' => 'Rlgn',
            'occptn' => 'Occptn',
            'bloodgrp' => 'Bloodgrp',
            'allergy' => 'Allergy',
            'addrpart' => 'Addrpart',
            'moopart' => 'Moopart',
            'tmbpart' => 'Tmbpart',
            'amppart' => 'Amppart',
            'chwpart' => 'Chwpart',
            'hometel' => 'Hometel',
            'pttype' => 'Pttype',
            'mrtlst' => 'Mrtlst',
            'spsname' => 'Spsname',
            'mthname' => 'Mthname',
            'fthname' => 'Fthname',
            'offaddr' => 'Offaddr',
            'offtel' => 'Offtel',
            'infmname' => 'Infmname',
            'prsnrlt' => 'Prsnrlt',
            'infmaddr' => 'Infmaddr',
            'infmtel' => 'Infmtel',
            'opdlct' => 'Opdlct',
            'fdate' => 'Fdate',
            'ldate' => 'Ldate',
            'pop_id' => 'เลขบัตรประชาชน',
            'height' => 'Height',
            'register' => 'Register',
            'pathimage' => 'Pathimage',
            'typearea' => 'Typearea',
            'statusinfo' => 'Statusinfo',
            'house_id' => 'House ID',
            'housetype' => 'Housetype',
            'viptype' => 'Viptype',
            'cidlabor' => 'Cidlabor',
            'engfname' => 'Engfname',
            'englname' => 'Englname',
            'engpname' => 'Engpname',
            'familyno' => 'Familyno',
            'father_cid' => 'Father Cid',
            'mother_cid' => 'Mother Cid',
            'couple_cid' => 'Couple Cid',
            'family_pos' => 'Family Pos',
            'movein' => 'Movein',
            'moveout' => 'Moveout',
            'typedisch' => 'Typedisch',
            'labor' => 'Labor',
            'passport' => 'Passport',
            'vstatus' => 'Vstatus',
            'gencid' => 'Gencid',
            'education' => 'Education',
            'couple' => 'Couple',
        ];
    }

    public function getAge()
    {
        $dob = new DateTime($this->brthdate);
        $today = new DateTime('today');
        $age = $dob->diff($today)->y;
        return $age.' ปี';
    }

    public function getFullname()
    {
        $dob = new DateTime($this->brthdate);
        $today = new DateTime('today');
        $age = $dob->diff($today)->y;
        if($this->pname == '') {
            if($this->male == '1' && $age < 15 ) {
                $this->mrtlst <> '6' ? $this->pname = 'ด.ช.': $this->pname = 'เณร';
            }  
            if($this->male == '1' && $age >= 15 ) {
                $this->mrtlst <> '6' ? $this->pname = 'นาย': $this->pname = 'พระ';
            }
            if($this->male == '2' && $age >= 15 ) {
                $this->mrtlst == '2' ? $this->pname = 'นาง': $this->pname = 'นางสาว';
            }
            if($this->male == '2' && $age >= 15 && $this->mrtlst == '6' ) {
                $this->pname = 'แม่ชี';
            }  
            if($this->male == '2' && $age < 15 ) {
                $this->pname = 'ด.ญ.';
            }
        }  
        return $this->pname.$this->fname.' '.$this->lname;
    }
    
    public function getJob()
    {
        return $this->hasOne(Occptn::className(),['occptn' => 'occptn']);
    }

    public function getฺBirthdate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->brthdate);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $result = $d.' '.$m.' '.$y;
        return $result;
    }

    public function getGender(){
        return $this->male == '1' ? 'ชาย':'หญิง';
    }

    public function getType()
    {
        return $this->hasOne(Pttype::className(),['pttype' => 'pttype']);
    }

}
