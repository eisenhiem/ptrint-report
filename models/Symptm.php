<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "symptm".
 *
 * @property int $vn
 * @property string $symptom
 * @property int $id
 */
class Symptm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'symptm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'symptom'], 'required'],
            [['vn'], 'integer'],
            [['symptom'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'symptom' => 'Symptom',
            'id' => 'ID',
        ];
    }
}
