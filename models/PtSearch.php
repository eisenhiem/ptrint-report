<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pt;

/**
 * PtSearch represents the model behind the search form of `app\models\Pt`.
 */
class PtSearch extends Pt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn', 'truebrth'], 'integer'],
            [['pname', 'fname', 'lname', 'brthdate', 'dthdate', 'male', 'ctzshp', 'ntnlty', 'rlgn', 'occptn', 'bloodgrp', 'allergy', 'addrpart', 'moopart', 'tmbpart', 'amppart', 'chwpart', 'hometel', 'pttype', 'mrtlst', 'spsname', 'mthname', 'fthname', 'offaddr', 'offtel', 'infmname', 'prsnrlt', 'infmaddr', 'infmtel', 'opdlct', 'fdate', 'ldate', 'pop_id', 'register', 'pathimage', 'typearea', 'statusinfo', 'house_id', 'housetype', 'viptype', 'cidlabor', 'engfname', 'englname', 'engpname', 'familyno', 'father_cid', 'mother_cid', 'couple_cid', 'family_pos', 'movein', 'moveout', 'typedisch', 'labor', 'passport', 'vstatus', 'gencid', 'education', 'couple'], 'safe'],
            [['height'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hn' => $this->hn,
            'brthdate' => $this->brthdate,
            'truebrth' => $this->truebrth,
            'dthdate' => $this->dthdate,
            'fdate' => $this->fdate,
            'ldate' => $this->ldate,
            'height' => $this->height,
            'movein' => $this->movein,
            'moveout' => $this->moveout,
        ]);

        $query->andFilterWhere(['like', 'pname', $this->pname])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'male', $this->male])
            ->andFilterWhere(['like', 'ctzshp', $this->ctzshp])
            ->andFilterWhere(['like', 'ntnlty', $this->ntnlty])
            ->andFilterWhere(['like', 'rlgn', $this->rlgn])
            ->andFilterWhere(['like', 'occptn', $this->occptn])
            ->andFilterWhere(['like', 'bloodgrp', $this->bloodgrp])
            ->andFilterWhere(['like', 'allergy', $this->allergy])
            ->andFilterWhere(['like', 'addrpart', $this->addrpart])
            ->andFilterWhere(['like', 'moopart', $this->moopart])
            ->andFilterWhere(['like', 'tmbpart', $this->tmbpart])
            ->andFilterWhere(['like', 'amppart', $this->amppart])
            ->andFilterWhere(['like', 'chwpart', $this->chwpart])
            ->andFilterWhere(['like', 'hometel', $this->hometel])
            ->andFilterWhere(['like', 'pttype', $this->pttype])
            ->andFilterWhere(['like', 'mrtlst', $this->mrtlst])
            ->andFilterWhere(['like', 'spsname', $this->spsname])
            ->andFilterWhere(['like', 'mthname', $this->mthname])
            ->andFilterWhere(['like', 'fthname', $this->fthname])
            ->andFilterWhere(['like', 'offaddr', $this->offaddr])
            ->andFilterWhere(['like', 'offtel', $this->offtel])
            ->andFilterWhere(['like', 'infmname', $this->infmname])
            ->andFilterWhere(['like', 'prsnrlt', $this->prsnrlt])
            ->andFilterWhere(['like', 'infmaddr', $this->infmaddr])
            ->andFilterWhere(['like', 'infmtel', $this->infmtel])
            ->andFilterWhere(['like', 'opdlct', $this->opdlct])
            ->andFilterWhere(['like', 'pop_id', $this->pop_id])
            ->andFilterWhere(['like', 'register', $this->register])
            ->andFilterWhere(['like', 'pathimage', $this->pathimage])
            ->andFilterWhere(['like', 'typearea', $this->typearea])
            ->andFilterWhere(['like', 'statusinfo', $this->statusinfo])
            ->andFilterWhere(['like', 'house_id', $this->house_id])
            ->andFilterWhere(['like', 'housetype', $this->housetype])
            ->andFilterWhere(['like', 'viptype', $this->viptype])
            ->andFilterWhere(['like', 'cidlabor', $this->cidlabor])
            ->andFilterWhere(['like', 'engfname', $this->engfname])
            ->andFilterWhere(['like', 'englname', $this->englname])
            ->andFilterWhere(['like', 'engpname', $this->engpname])
            ->andFilterWhere(['like', 'familyno', $this->familyno])
            ->andFilterWhere(['like', 'father_cid', $this->father_cid])
            ->andFilterWhere(['like', 'mother_cid', $this->mother_cid])
            ->andFilterWhere(['like', 'couple_cid', $this->couple_cid])
            ->andFilterWhere(['like', 'family_pos', $this->family_pos])
            ->andFilterWhere(['like', 'typedisch', $this->typedisch])
            ->andFilterWhere(['like', 'labor', $this->labor])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'vstatus', $this->vstatus])
            ->andFilterWhere(['like', 'gencid', $this->gencid])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'couple', $this->couple]);

        return $dataProvider;
    }
}
