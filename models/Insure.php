<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insure".
 *
 * @property int $hn
 * @property string $pop_id
 * @property string $card_id
 * @property string $pttype
 * @property string $datein
 * @property string $dateexp
 * @property string $hospmain
 * @property string $hospsub
 * @property string $note
 * @property string $notedate
 * @property int $id
 */
class Insure extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insure';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn', 'pop_id', 'card_id', 'pttype', 'hospmain', 'hospsub', 'note'], 'required'],
            [['hn'], 'integer'],
            [['datein', 'dateexp', 'notedate'], 'safe'],
            [['pop_id'], 'string', 'max' => 13],
            [['card_id'], 'string', 'max' => 17],
            [['pttype'], 'string', 'max' => 2],
            [['hospmain', 'hospsub'], 'string', 'max' => 5],
            [['note'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'Hn',
            'pop_id' => 'Pop ID',
            'card_id' => 'Card ID',
            'pttype' => 'Pttype',
            'datein' => 'Datein',
            'dateexp' => 'Dateexp',
            'hospmain' => 'Hospmain',
            'hospsub' => 'Hospsub',
            'note' => 'Note',
            'notedate' => 'Notedate',
            'id' => 'ID',
        ];
    }

    public function getHmain()
    {
        return $this->hasOne(Hospcode::className(),['off_id' => 'hospmain']);
    }

    public function getHospName()
    {
        return $this->hmain->namehosp;
    }

}
