<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dtdx".
 *
 * @property int $id
 * @property int $dn
 * @property int $dtxtime
 * @property string $area
 * @property string $icdda
 * @property string $dttx
 * @property int $charge
 * @property int $rcptno
 * @property int|null $consultid
 * @property int|null $codecdt2id
 */
class Dtdx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dtdx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dn', 'dtxtime', 'area', 'icdda', 'dttx', 'charge', 'rcptno'], 'required'],
            [['dn', 'dtxtime', 'charge', 'rcptno', 'consultid', 'codecdt2id'], 'integer'],
            [['area'], 'string', 'max' => 3],
            [['icdda'], 'string', 'max' => 6],
            [['dttx'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dn' => 'Dn',
            'dtxtime' => 'Dtxtime',
            'area' => 'Area',
            'icdda' => 'Icdda',
            'dttx' => 'Dttx',
            'charge' => 'Charge',
            'rcptno' => 'Rcptno',
            'consultid' => 'Consultid',
            'codecdt2id' => 'Codecdt2id',
        ];
    }
}
